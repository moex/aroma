/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   IntensiteImplicationEntropique.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.measures.cardbased;

public class IntensiteImplicationEntropique extends IntensiteImplication {

    private static double OMEGA = 2.0;

    public IntensiteImplicationEntropique() {
	super();

    }

    public double getIndice(int na, int nb, int nab, int n) {

	return Math.sqrt(super.getIndice(na, nb, nab, n)
		* inclusion(na, nb, nab, n, OMEGA));
    }

    private double I(int na, int nab, double omega) {
	if ((na - nab) < ((double) na / 2)) {
	    double r1 = ((double) nab / na);
	    double r2 = ((double) (na - nab) / na);
	    return 1.0 - Math.pow(-r1 * log2(r1) - r2 * log2(r2), omega);
	}
	return 0;
    }

    private double inclusion(int na, int nb, int nab, int n, double omega) {
	return Math.pow(
		I(na, nab, omega) * I(n - nb, n - na - nb + nab, omega),
		1.0 / (2 * omega));
    }

}
