/**
 *   This file is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.utils.ling;

/**
 * @author jerome
 *
 */
public abstract class StringTransform {

    
    
    public final static String addSpaces(String s)  {
	StringBuffer bf = new StringBuffer();
	char previous=' ';
	for (int i=0 ; i<s.length() ; i++) {
	    char c = s.charAt(i);
	    if (previous != ' ') {
		if (Character.isLowerCase (previous) && (Character.isUpperCase(c)||Character.isDigit(c))) {
		    bf.append(' ');
		}
		else if (Character.isDigit(previous) && Character.isLetter(c)) {
		    bf.append(' ');
		}
	    }
	    bf.append(c);
	    previous=c;
	}
	return bf.toString();
    }
}
