/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   StemmedTermExtractor.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.utils.ling;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.StopAnalyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.ar.ArabicAnalyzer;
import org.apache.lucene.analysis.cz.CzechAnalyzer;
import org.apache.lucene.analysis.de.GermanAnalyzer;
import org.apache.lucene.analysis.el.GreekAnalyzer;
import org.apache.lucene.analysis.fr.ElisionFilter;
import org.apache.lucene.analysis.fr.FrenchAnalyzer;
import org.apache.lucene.analysis.nl.DutchAnalyzer;
import org.apache.lucene.analysis.snowball.SnowballAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.TermAttribute;
import org.apache.lucene.util.Version;

public class StemmedTermExtractor implements TermExtractor {
    
    private Pattern upperCaseFilter = Pattern.compile("(\\p{Lower})(\\p{Upper})");
    
    private final Map<String,Analyzer> analysers = new HashMap<String,Analyzer>();
    private final Analyzer otherLanguageAnalyzer = new StandardAnalyzer(Version.LUCENE_30,Collections.emptySet());
    
    private final HashSet<String> stopwords;
    

    public StemmedTermExtractor() {
	Analyzer englishA = new SnowballAnalyzer(Version.LUCENE_30,"English", StopAnalyzer.ENGLISH_STOP_WORDS_SET);
	analysers.put("en", englishA);
	analysers.put("fr", new FrenchAnalyzer(Version.LUCENE_30));
	analysers.put("ar", new ArabicAnalyzer(Version.LUCENE_30));
	analysers.put("cz", new CzechAnalyzer(Version.LUCENE_30));
	analysers.put("de", new GermanAnalyzer(Version.LUCENE_30));
	analysers.put("el", new GreekAnalyzer(Version.LUCENE_30));
	analysers.put("nl", new DutchAnalyzer(Version.LUCENE_30));
	
	// DEFAULT analyser when no language
	analysers.put(null, englishA);
	
	String[] stops = new String[] {"ja_fi","agus_ga","un_lv","in_sl", "og_da","és_hu","ir_lt","ja_et"};
	stopwords = new HashSet<String>(stops.length);
	for (String s : stops) stopwords.add(s);
    }
    
    
    public String getTermType() {
	return "single and binary";
    }

    public Set<String> getTerms(String text) {
	return getTerms(text,null);
    }
    
    public Set<String> getTerms(String text, String language) {
	if (text == null)
	    return Collections.emptySet();
	
	String suffix="";
	if (language!=null) suffix="_"+language;
	//text=upperCaseFilter.matcher(text).replaceAll("$1 $2");
	//text=text.replace('_', ' ');
	//System.out.println(text);
	Set<String> terms = new HashSet<String>();
	try {
	    Analyzer analyser = analysers.get(language);
	    if (analyser==null) analyser=otherLanguageAnalyzer;
	    TokenStream ts = analyser.reusableTokenStream("", new StringReader(text));
	    TermAttribute termAtt = ts.addAttribute(TermAttribute.class);
	    String previous = null;
	    while (ts.incrementToken()) {
		String aWord = termAtt.term();
		if (aWord.length() > 1) {
		    String current = aWord+suffix;
		    if (!stopwords.contains(current)) {
			terms.add(current);
        		if (previous != null)
        		    terms.add((previous + " " + current));
        		previous = aWord;
		    }
		}
	    }
	    ts.end();
	} catch (IOException e) {
	    e.printStackTrace();
	}
	
	//System.err.println("--------------------"+language+"--------------------");
	//for (String t : terms) {System.err.println(t);}
	return terms;
    }
    
    public String getStemmedText(String text, String language) {
	if (text == null)
	    return null;
	StringBuffer res = new StringBuffer();
	try {
	    Analyzer analyser = analysers.get(language);
	    if (analyser==null) analyser=otherLanguageAnalyzer;
	    TokenStream ts = analyser.reusableTokenStream("", new StringReader(text));
	    TermAttribute termAtt = ts.addAttribute(TermAttribute.class);
	    while (ts.incrementToken()) {
		String current = termAtt.term();
		if (!stopwords.contains(current)) {
		    res.append(current);
		    res.append(' ');
		}
	    }
	    ts.end();
	    if (res.length()>0)
		res.deleteCharAt(res.length()-1);
	} catch (IOException e) {
	    e.printStackTrace();
	}
	return res.toString();
    }

    public void free() {
	for (Analyzer a : analysers.values())
	   try {
	    a.close();
	   }
	catch (NullPointerException e) {}
	analysers.clear();
    }
}
