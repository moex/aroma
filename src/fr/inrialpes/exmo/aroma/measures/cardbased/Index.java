/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   Index.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.measures.cardbased;

/**
 * @author jerome
 * 
 *         To change the template for this generated type comment go to
 *         Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public abstract class Index {

    protected static double log2(double x) {
	return Math.log(x) / Math.log(2);
    }

    protected static double factorial(double n) {
	double result = 1;

	if (n <= 15) {
	    // result = 1;
	    for (int k = 0; k <= n; k++) {
		result = result * k;
	    }
	} else {
	    result = Math.pow(n, n) * Math.exp(-n) * Math.sqrt(2 * Math.PI * n);
	}
	return result;
    }

    // private Indice(){};

    public abstract double getIndice(int na, int nb, int nab, int n);

    /*
     * public static Indice getInstance() { retun }
     */

}
