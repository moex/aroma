/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   AbstractAEntityFactory.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.ontology.entities;

import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import fr.inrialpes.exmo.aroma.utils.ling.StemmedTermExtractor;
import fr.inrialpes.exmo.aroma.utils.ling.StringTransform;
import fr.inrialpes.exmo.aroma.utils.ling.TermExtractor;
import fr.inrialpes.exmo.ontowrap.Annotation;
import fr.inrialpes.exmo.ontowrap.HeavyLoadedOntology;
import fr.inrialpes.exmo.ontowrap.LoadedOntology;
import fr.inrialpes.exmo.ontowrap.OntowrapException;

public abstract class AbstractAEntityFactory {

    protected Set<URI> forbiddenURIs;
    protected final TermExtractor te;

    protected Map<URI, AEntity> classHierarchies;
    protected Map<URI, AEntity> propertyHierarchies;
    
    public AbstractAEntityFactory(boolean useCache) {
	this(useCache,null);
    }

    public AbstractAEntityFactory(boolean useCache, Set<URI> forbiddenURIs) {
	this.forbiddenURIs=forbiddenURIs;
	te = new StemmedTermExtractor();
	if (useCache) {
	    classHierarchies = new WeakHashMap<URI, AEntity>();
	    propertyHierarchies = new WeakHashMap<URI, AEntity>();
	}
    }
    
    public void setForbiddenURIs(Set<URI> forbiddenURIs) {
	this.forbiddenURIs=forbiddenURIs;
    }

    public AEntity getClassHierarchy(HeavyLoadedOntology<?> onto) {
	if (classHierarchies != null && classHierarchies.containsKey(onto.getURI()))
	    return classHierarchies.get(onto.getURI());
	AEntityImpl root = createARoot(onto);
	try {
        	Map<URI, AEntityImpl> classes = buildClasses(onto);
        	buildHierarchy(classes, onto);
        	// Assign root
        	for (AEntity aClass : classes.values()) {
        	    if (aClass.getSuperEntities(true).size() == 0) {
        		root.addChild(aClass);
        	    }
        	}
        	if (classHierarchies != null)
        	    classHierarchies.put(onto.getURI(), root);
	}
	catch (OntowrapException e) {
	    e.printStackTrace();
	}
	return root;
    }

    public AEntity getPropertyHierarchy(HeavyLoadedOntology<?> onto) {
	if (propertyHierarchies != null && propertyHierarchies.containsKey(onto.getURI()))
	    return propertyHierarchies.get(onto.getURI());

	AEntityImpl root = createARoot(onto);
	try {
        	Map<URI, AEntityImpl> properties = buildProperties(onto);
        	buildHierarchy(properties, onto);
        	// Assign root
        	for (AEntity aProp : properties.values()) {
        	    if (aProp.getSuperEntities(true).size() == 0) {
        		root.addChild(aProp);
        	    }
        	}
        	if (propertyHierarchies != null)
        	    propertyHierarchies.put(onto.getURI(), root);
    	}
	catch (OntowrapException e) {
	    e.printStackTrace();
	}
	
	return root;
    }

    protected void buildHierarchy(Map<URI, AEntityImpl> entities, HeavyLoadedOntology<?> onto) {
	for (URI entURI : entities.keySet()) {
	    for (URI supURI : getSuperEntities(entURI, onto)) {
		if (entities.get(supURI) != null) {
		    entities.get(entURI).addParent(entities.get(supURI));
		    // System.err.println(entURI+" : "+supURI);
		}
	    }
	}
    }

    
    protected boolean isForbidden(URI uri) {
	if (forbiddenURIs==null) return false;
	for (URI f : forbiddenURIs) {
	    if (uri.toString().startsWith(f.toString())) return true;
	}
	return false;
    }
    
    
    protected Map<URI, AEntityImpl> buildClasses(HeavyLoadedOntology<?> onto) throws OntowrapException {
	Map<URI, AEntityImpl> entities = new HashMap<URI, AEntityImpl>();
	for (Object aClass : onto.getClasses()) {
	    try {
		URI entURI = onto.getEntityURI(aClass);
		if (!isForbidden(entURI)) {
		    entities.put(onto.getEntityURI(aClass), buildEntity(onto,aClass));
		}
		
	    } catch (OntowrapException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}
	return entities;
    }

    protected Map<URI, AEntityImpl> buildProperties(HeavyLoadedOntology<?> onto) throws OntowrapException {
	Map<URI, AEntityImpl> entities = new HashMap<URI, AEntityImpl>();
	for (Object aProp : onto.getProperties()) {
	    try {
		URI entURI = onto.getEntityURI(aProp);
		if (!isForbidden(entURI)) {
		    entities.put(onto.getEntityURI(aProp), buildEntity(onto, aProp));
		}
	    } catch (OntowrapException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}
	return entities;
    }

    protected AEntityImpl buildEntity(HeavyLoadedOntology<?> onto, Object aClass) throws OntowrapException {
	AEntityImpl entity = new AEntityImpl(onto, aClass);
	entity.addTerms(getAnnotationsTerms(onto, aClass));

	
	// Terms extraction depending of the entity type
	Set<Annotation> values = null;
	if (onto.isClass(aClass)) {
	    values = getClassSpecificTerms(aClass, onto);
	} else if (onto.isObjectProperty(aClass)) {
	    values = getObjectPSpecificTerms(aClass, onto);
	} else if (onto.isDataProperty(aClass)) {
	    values = getDataPSpecificTerms(aClass, onto);
	}

	// Is it interesting ???
	for (Annotation aString : values) {
	    // if (aString.indexOf(' ') > -1 && aString.indexOf(' ') <
	    // aString.lastIndexOf(' '))
	    entity.addTerms(te.getTerms(aString.getValue(),aString.getLanguage()));
	    // else
	    // entity.addTerm(aString.toLowerCase());
	}
	return entity;
    }

    protected Set<String> getAnnotationsTerms(LoadedOntology<?> onto, Object anEntity) {
	// Terms extraction common to all entity type
	Set<String> terms = new HashSet<String>();
	try {
	    String localName = onto.getEntityURI(anEntity).getFragment();
	    if (localName!=null)
		terms.addAll(te.getTerms(StringTransform.addSpaces(localName)));
	    //terms.add(onto.getEntityURI(anEntity).getFragment());
	    //terms.addAll(onto.getEntityNames(anEntity));
	    //System.out.println("annotations for "+anEntity);
	    for (Annotation annot : onto.getEntityAnnotationsL(anEntity)) {
		//System.out.println(annot);
		terms.addAll(te.getTerms(annot.getValue(),annot.getLanguage()));
	    }
	} catch (OntowrapException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return terms;
    }

    protected abstract Set<Annotation> getClassSpecificTerms(Object aClass, HeavyLoadedOntology<?> onto);

    protected abstract Set<Annotation> getObjectPSpecificTerms(Object aClass, LoadedOntology<?> onto);

    protected abstract Set<Annotation> getDataPSpecificTerms(Object aClass, LoadedOntology<?> onto);

    protected abstract AEntityImpl createARoot(LoadedOntology<?> onto);

    protected abstract Set<URI> getSuperEntities(URI entity, HeavyLoadedOntology<?> onto);
    
    public void free() {
	te.free();
    }

}
