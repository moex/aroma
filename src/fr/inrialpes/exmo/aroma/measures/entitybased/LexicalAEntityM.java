/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   LexicalAEntityM.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.measures.entitybased;

import java.util.HashSet;
import java.util.Set;

import fr.inrialpes.exmo.aroma.ontology.entities.AEntity;
import fr.inrialpes.exmo.ontosim.Measure;

public class LexicalAEntityM implements Measure<AEntity> {

    public double getDissim(AEntity o1, AEntity o2) {
	return 1 - getSim(o1, o2);
    }

    public fr.inrialpes.exmo.ontosim.Measure.TYPES getMType() {
	return Measure.TYPES.similarity;
    }

    public double getMeasureValue(AEntity o1, AEntity o2) {
	Set<String> e1T = new HashSet<String>();
	e1T.addAll(o1.getTerms());
	for (AEntity c : o1.getSubEntities(true)) {
	    e1T.removeAll(c.getTerms());
	}

	Set<String> e2T = new HashSet<String>();
	e2T.addAll(o2.getTerms());
	for (AEntity c : o2.getSubEntities(true)) {
	    e2T.removeAll(c.getTerms());
	}

	int na = e1T.size();
	int nb = e2T.size();
	e1T.retainAll(e2T);
	int nab = e1T.size();

	if (na == 0 || nb == 0)
	    return 0;
	double val = ((double) nab) / Math.min(na, nb);
	// System.out.println(val);
	return val;
    }

    public double getSim(AEntity o1, AEntity o2) {
	return getMeasureValue(o1, o2);
    }

}
