/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   SBMeasure.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.measures.entitybased;

import java.util.Set;

import fr.inrialpes.exmo.aroma.alignment.MatchingRelation;
import fr.inrialpes.exmo.aroma.ontology.entities.AEntity;
import fr.inrialpes.exmo.ontosim.Measure;
import fr.inrialpes.exmo.ontosim.util.matrix.MatrixDouble;

/**
 * @author jerome
 * 
 */
public class SBMeasure implements Measure<AEntity> {

    protected MatrixDouble<AEntity, AEntity> vals;

    public SBMeasure(Set<MatchingRelation> mrSet) {
	vals = new MatrixDouble<AEntity, AEntity>();
	for (MatchingRelation mr : mrSet)
	    vals.put(mr.getObject1(), mr.getObject2(), mr.getStrength());
    }

    protected double getSim(Set<AEntity> s1, Set<AEntity> s2) {
	int nb = 0;
	double sim = 0;
	for (AEntity p1 : s1) {
	    for (AEntity p2 : s2) {
		double val = vals.get(p1, p2);
		if (!Double.isNaN(val)) {
		    sim += val;
		    nb++;
		}
	    }
	}
	if (nb == 0)
	    return Double.NaN;
	return sim / nb;
    }

    public double getMeasureValue(AEntity o1, AEntity o2) {
	double simParents = getSim(o1.getSuperEntities(true), o2
		.getSuperEntities(true));
	double simChildren = getSim(o1.getSubEntities(true), o2
		.getSubEntities(true));
	if (Double.isNaN(simParents) && Double.isNaN(simChildren))
	    return 0;
	if (Double.isNaN(simParents))
	    return simChildren;
	if (Double.isNaN(simChildren))
	    return simParents;
	return (simParents + simChildren) / 2;
    }

    public double getDissim(AEntity o1, AEntity o2) {
	return 1 - getSim(o1, o2);
    }

    public double getSim(AEntity o1, AEntity o2) {
	return getMeasureValue(o1, o2);
    }

    public TYPES getMType() {
	return TYPES.similarity;
    }

}
