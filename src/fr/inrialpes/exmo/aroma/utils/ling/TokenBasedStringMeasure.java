/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   TokenBasedStringMeasure.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.utils.ling;

import com.wcohen.ss.JaroWinkler;

import fr.inrialpes.exmo.ontosim.Measure;
import fr.inrialpes.exmo.ontosim.set.MaxCoupling;
import fr.inrialpes.exmo.ontosim.set.SetMeasure;
import fr.inrialpes.exmo.ontosim.string.StringMeasureSS;

public class TokenBasedStringMeasure implements Measure<String> {

    private TermExtractor te;

    private Measure<String> stringMeasure;

    private SetMeasure<String> setMeasure;

    public TokenBasedStringMeasure() {
	stringMeasure = new StringMeasureSS(new JaroWinkler());
	setMeasure = new MaxCoupling<String>(stringMeasure, 0);
	te = new VerySimpleTE();
    }

    public double getDissim(String o1, String o2) {
	return setMeasure.getDissim(te.getTerms(o1), te.getTerms(o2));
    }

    public double getMeasureValue(String o1, String o2) {
	return setMeasure.getMeasureValue(te.getTerms(o1), te.getTerms(o2));
    }

    public double getSim(String o1, String o2) {
	return setMeasure.getSim(te.getTerms(o1), te.getTerms(o2));
    }

    public fr.inrialpes.exmo.ontosim.Measure.TYPES getMType() {
	return setMeasure.getMType();
    }

}
