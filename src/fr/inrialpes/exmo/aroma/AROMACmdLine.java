/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   AROMACmdLine.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma;

import fr.inrialpes.exmo.align.impl.ObjectAlignment;
import fr.inrialpes.exmo.align.impl.renderer.RDFRendererVisitor;
import gnu.getopt.Getopt;
import gnu.getopt.LongOpt;

import java.io.File;
import java.io.PrintWriter;
import java.net.URI;
import java.util.Properties;

public class AROMACmdLine {

    private static double lexicalThreshold=0.8;
    
    public static void printUsage() {
	System.err.println("java AROMACmdLine onto1.owl onto2.owl [res_file.rdf]");
	System.err.println("or java -jar aroma.jar onto1.owl onto2.owl [res_file.rdf]");
	System.err.println("where the options are:");
	System.err.println("\t--consensus=n -c n set the minumum number of annotationd that two entities must share to be matched by equality matcher");
	System.err.println("\t--forbiddenUris=uri1,uri2,... -u uri1,uri2,... entities uris which begin with uri1,uri2,... are not considered");
	System.err.println("\t--output=filename -o filename Output the results in filename (stdout by default)");
	System.err.println("\t--lexicalThreshold=n -l n use the threshold n for the syntax matching procedure. Negative values disable threshold matching");
	System.err.println("\t--ruleThreshold=n -r n use the threshold n for the rule matching procedure");
	System.err.println("\t--skos ontologies are provided in skos language");
	System.err.println("\t--help -h                       Print this message");
	System.exit(-1);
    }
    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
	Properties p = new Properties();
	
	LongOpt[] longopts = new LongOpt[8];
	
	longopts[0] = new LongOpt("help", LongOpt.NO_ARGUMENT, null, 'h');
	longopts[1] = new LongOpt("outputfile", LongOpt.REQUIRED_ARGUMENT, null, 'o'); 
	longopts[2] = new LongOpt("forbiddenUris", LongOpt.REQUIRED_ARGUMENT, null, 'u'); 
	longopts[3] = new LongOpt("lexicalThreshold", LongOpt.REQUIRED_ARGUMENT, null, 'l');
	longopts[4] = new LongOpt("instances1", LongOpt.REQUIRED_ARGUMENT, null, 'i');
	longopts[5] = new LongOpt("skos", LongOpt.NO_ARGUMENT, null, 's');
	longopts[6] = new LongOpt("ruleThreshold", LongOpt.REQUIRED_ARGUMENT, null, 'r');
	longopts[6] = new LongOpt("consensus", LongOpt.REQUIRED_ARGUMENT, null, 'c');
	

	Getopt opts = new Getopt(AROMACmdLine.class.getCanonicalName(), args, "l:u:o:i:r:c:sh", longopts);
	int c;
	while ((c = opts.getopt()) != -1)
	    switch (c) {
	    case 'h' : 	
		printUsage();
		return;
	    case 'u' :
		p.setProperty("forbiddenUris", opts.getOptarg());
		break;
	    case 'l' :
		double t=lexicalThreshold;
		try {
		    t=Double.parseDouble(opts.getOptarg());
		    if (t<0) {
			p.setProperty("lexicalSim", "false");
		    }
		    if (t >1) {
		    	System.err.println(opts.getOptarg()+" value must be between 0 and 1, it will use "+lexicalThreshold+" value instead");
		    	t=lexicalThreshold;
		    }
		}
		catch (NumberFormatException e) {
		    System.err.println(opts.getOptarg()+" is not a valid number, it will use "+lexicalThreshold);
		}
		p.setProperty("LexicalSimThreshold",String.valueOf(t));
		break;
	    case 'i' :
		p.setProperty("instances1", opts.getOptarg());
		break;
	    case 's' :
		p.setProperty("skos", "true");
		break;
	    case 'r' :
		double r=Double.NaN;
		try {
		    r=Double.parseDouble(opts.getOptarg());
		    if (r < 0 || r >1) {
		    	System.err.println(opts.getOptarg()+" value must be between 0 and 1, it will use automatic value instead");
		    	r=Double.NaN;
		    }
		}
		catch (NumberFormatException e) {
		    System.err.println(opts.getOptarg()+" is not a valid number, it will use use automatic value instead");
		}
		p.setProperty("RuleThreshold", String.valueOf(r));
		break;
	    case 'c' :
		try {
		    int consensus = Integer.parseInt(opts.getOptarg());
		    if (consensus>0)
			p.setProperty("consensus", String.valueOf(consensus));
		}
		catch (NumberFormatException e) {
		    System.err.println(opts.getOptarg()+" is not a valid number, it will use use automatic value instead");
		}
	
	    }
	
	// test parameters 
	int a = opts.getOptind();
	if (args.length<a+2) {
	    printUsage();
	}
	

	URI ontoURI1 = (new File(args[a])).toURI();
	URI ontoURI2 = (new File(args[a+1])).toURI();

	PrintWriter pw = new PrintWriter(System.out);
	if (args.length>=a+3) {
	    File output = new File(args[a+2]);
	    pw = new PrintWriter(output,"UTF-8");
	}

	
	AROMA align = new AROMA();
	align.skos="true".equals(p.getProperty("skos"));
	align.init(ontoURI1, ontoURI2);
	align.align(new ObjectAlignment(), p);

	RDFRendererVisitor rrv = new RDFRendererVisitor(pw);
	rrv.visit(align);
	pw.close();

    }

}
