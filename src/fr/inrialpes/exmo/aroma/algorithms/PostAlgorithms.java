/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   PostAlgorithms.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.algorithms;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.semanticweb.owl.align.AlignmentException;

import fr.inrialpes.exmo.aroma.alignment.AlignmentFactory;
import fr.inrialpes.exmo.aroma.alignment.Equivalence;
import fr.inrialpes.exmo.aroma.alignment.Implication;
import fr.inrialpes.exmo.aroma.alignment.MatchingRelation;
import fr.inrialpes.exmo.aroma.ontology.entities.AEntity;
import fr.inrialpes.exmo.ontosim.util.HungarianAlgorithm;
import fr.inrialpes.exmo.ontosim.util.matrix.MatrixDouble;
import fr.inrialpes.exmo.ontosim.util.matrix.MatrixDoubleArray;

public class PostAlgorithms {

    public static Set<MatchingRelation> deduceEquivalences(Set<MatchingRelation> align, Set<Implication> st, Set<Implication> ts, AlignmentFactory af) {

	/* A more (time) efficient implementation
	
	Map<AEntity,Map<AEntity,Implication>> idx = new HashMap<AEntity,Map<AEntity,Implication>>();
	for (Implication imp1 : st) {
	    Map<AEntity,Implication> subIdx = idx.get(imp1.getObject2());
	    if (subIdx==null) {
		subIdx = new HashMap<AEntity,Implication>();
		idx.put(imp1.getObject2(), subIdx);
	    }
	    subIdx.put(imp1.getObject1(), imp1);
	}
	
	Set<Implication> toRemove = new HashSet<Implication>();
	for (Implication imp2 : ts) {
	    Map<AEntity,Implication> subIdx = idx.get(imp2.getObject1());
	    if (subIdx!=null) {
		Implication imp1 = subIdx.get(imp2.getObject1());
		if (imp1!=null) {
		    Equivalence eq = af.getEquivalence(imp1, imp2);
			if (eq != null) {
			    align.add(eq);
			    toRemove.add(imp1);
			    toRemove.add(imp2);
			}
		}
	    }
	}*/
	

	Set<Implication> toRemove = new HashSet<Implication>();
	for (Implication imp1 : st) {
	    for (Implication imp2 : ts) {
		Equivalence eq = af.getEquivalence(imp1, imp2);
		if (eq != null) {
		    align.add(eq);
		    toRemove.add(imp1);
		    toRemove.add(imp2);
		    break;
		}
	    }
	}
	
	
	align.addAll(st);
	align.addAll(ts);
	align.removeAll(toRemove);
	return align;
    }

    public static Set<MatchingRelation> removeRedundancy(Set<MatchingRelation> smr) {
	Set<MatchingRelation> redundant = new HashSet<MatchingRelation>();
	for (MatchingRelation mr : smr) {
	    if (mr instanceof Implication) {
		Implication imp = (Implication) mr;
		for (MatchingRelation mr2 : smr) {
		    if (!imp.equals(mr2)) {
			if ((imp.getSource().isDescendantOf(mr2.getObject1()) && imp.getDest().isAncestorOf(mr2.getObject2()))
				|| (imp.getSource().isDescendantOf(mr2.getObject2()) && imp.getDest().isAncestorOf(mr2.getObject1()))) {
			    redundant.add(imp);
			    // smr.add(imp); ???
			    break;
			}
		    }
		}
	    }
	}

	smr.removeAll(redundant);
	return smr;
    }

    public static Set<MatchingRelation> removeCycles(Set<MatchingRelation> smr, AlignmentFactory af) {
	Set<MatchingRelation> toRemove = new HashSet<MatchingRelation>();
	Set<MatchingRelation> toAdd = new HashSet<MatchingRelation>();
	analyzeCycles(af, smr, smr, toRemove, toAdd);
	smr.removeAll(toRemove);
	while (toAdd.size() > 0) {
	    smr.addAll(toAdd);
	    Set<MatchingRelation> toAddCopy = new HashSet<MatchingRelation>(toAdd);
	    toRemove = new HashSet<MatchingRelation>();
	    toAdd = new HashSet<MatchingRelation>();
	    analyzeCycles(af, toAddCopy, smr, toRemove, toAdd);
	    analyzeCycles(af, smr, toAddCopy, toRemove, toAdd);
	    smr.removeAll(toRemove);
	}
	return smr;
    }

    private static void analyzeCycles(final AlignmentFactory af,
	    Set<MatchingRelation> s1, final Set<MatchingRelation> s2,
	    final Set<MatchingRelation> toRemove,
	    final Set<MatchingRelation> toAdd) {

	for (MatchingRelation mr : s1) {
	    for (MatchingRelation mr2 : s2) {
		// int size=toRemove.size();
		if (!mr.equals(mr2)) {
		    if (mr.getObject1().isAncestorOf(mr2.getObject1())
			    && mr.getObject2().isDescendantOf(mr2.getObject2())) {

			if (mr instanceof Equivalence && mr2 instanceof Equivalence) {
			    if (mr.getStrength() > mr2.getStrength())
				toRemove.add(mr2);
			    else if (mr.getStrength() < mr2.getStrength())
				toRemove.add(mr);
			    else if (mr.getObject2().equals(mr2.getObject2()))
				toRemove.add(mr2);
			    else if (mr.getObject1().equals(mr2.getObject1()))
				toRemove.add(mr);
			} else if (mr instanceof Equivalence
				&& mr2.getObject1() == ((Implication) mr2)
					.getDest())
			    toRemove.add(mr2);
			else if (mr2 instanceof Equivalence
				&& mr.getObject2() == ((Implication) mr)
					.getDest()) {
			    toRemove.add(mr);
			} else if (mr instanceof Implication
				&& mr2 instanceof Implication
				&& mr.getObject2() == ((Implication) mr)
					.getDest()
				&& mr2.getObject1() == ((Implication) mr2)
					.getDest()) {
			    toRemove.add(mr);
			    toRemove.add(mr2);
			    toAdd.add(af.getEquivalence(mr.getObject1(), mr2
				    .getObject2()));
			}
		    }
		}

	    }
	}
    }

    /*
     * public static void retain2(Set<MatchingRelation> s) {
     * Matrix<AEntity,AEntity> vals = new MatrixDouble<AEntity, AEntity>();
     * Set<AEntity> obj1 = new HashSet<AEntity>(); Set<AEntity> obj2 = new
     * HashSet<AEntity>(); List<AEntity> obj1R = new ArrayList<AEntity>();
     * List<AEntity> obj2R = new ArrayList<AEntity>(); for (MatchingRelation mr
     * : s) { vals.put(mr.getObject1(), mr.getObject2(), mr.getStrength()); if
     * (obj1.contains(mr.getObject1())) obj1R.add(mr.getObject1()); else
     * obj1.add(mr.getObject1()); if (obj2.contains(mr.getObject2()))
     * obj2R.add(mr.getObject2()); else obj2.add(mr.getObject2()); }
     * obj1.removeAll(obj1R); obj2.removeAll(obj2R);
     * 
     * 
     * 
     * }
     */

    /**
     * Hungrian algorithm which select the matching subset.
     * 
     * @param s
     * @return
     */
    public static Set<MatchingRelation> hSelect(Set<MatchingRelation> s) {
	MatrixDouble<String, String> values = new MatrixDouble<String, String>();
	Map<String, MatchingRelation> mapMR = new HashMap<String, MatchingRelation>();
	for (MatchingRelation mr : s) {
	    double val = mr.getStrength();
	    if (Double.isNaN(val))
		val = 0;
	    values.put(mr.getObject1().getURI().toString(), mr.getObject2()
		    .getURI().toString(), val);
	    mapMR.put(mr.getObject1().getURI().toString() + "-"
		    + mr.getObject2().getURI().toString(), mr);
	}

	boolean transposed = false;
	MatrixDoubleArray<String, String> mda;
	if (values.getDimR().size() > values.getDimC().size()) {
	    mda = values.toArrayT();
	    transposed = true;
	} else
	    mda = values.toArray();
	int[][] assignment = HungarianAlgorithm.hgAlgorithm(mda.getValues(),"max");
	Set<MatchingRelation> selected = new HashSet<MatchingRelation>();
	for (int i = 0; i < assignment.length; i++) {
	    String e1, e2;
	    if (transposed) {
		e2 = mda.getRows().get(assignment[i][0]);
		e1 = mda.getCols().get(assignment[i][1]);
	    } else {
		e1 = mda.getRows().get(assignment[i][0]);
		e2 = mda.getCols().get(assignment[i][1]);
	    }
	    MatchingRelation current = mapMR.get(e1 + "-" + e2);
	    if (current != null)
		selected.add(current);
	}
	return selected;
    }

    private static void addEntity2Rel(AEntity ent, MatchingRelation mr,
	    Map<AEntity, Set<MatchingRelation>> ent2Rel) {
	Set<MatchingRelation> rels1 = ent2Rel.get(ent);
	if (rels1 == null) {
	    rels1 = new HashSet<MatchingRelation>();
	    ent2Rel.put(ent, rels1);
	}
	rels1.add(mr);
    }

    /**
     * Algorithm which only retain, for each entity, the best relations. It
     * returns the set of relations containing entities appearing more than once
     * time in s
     * 
     * @param s
     *            the set of matching relations
     */
    public static Set<MatchingRelation> retainMax(Set<MatchingRelation> s, boolean functional, boolean bidirectional) {
	Set<MatchingRelation> maxNot11 = new HashSet<MatchingRelation>();
	Map<AEntity, Set<MatchingRelation>> ent2Rel = new HashMap<AEntity, Set<MatchingRelation>>();
	for (MatchingRelation mr : s) {
	    addEntity2Rel(mr.getObject1(), mr, ent2Rel);
	    if (bidirectional)
		addEntity2Rel(mr.getObject2(), mr, ent2Rel);
	}

	for (AEntity e : ent2Rel.keySet()) {
	    Set<MatchingRelation> max = new HashSet<MatchingRelation>();
	    for (MatchingRelation mr : ent2Rel.get(e)) {
		if (max.size() == 0 || mr.getStrength() > max.iterator().next().getStrength()) {
		    max.clear();
		    max.add(mr);
		} else if (mr.getStrength() == max.iterator().next().getStrength()) {
		    max.add(mr);
		}
	    }
	    if (max.size() > 1) {
		maxNot11.addAll(max);
	    }
	    if (!functional || max.size() == 1) {
		ent2Rel.get(e).removeAll(max);
	    }
	    s.removeAll(ent2Rel.get(e));
	}
	return maxNot11;
    }

    public static void removeLowRelations(Set<MatchingRelation> s, double seuil) {
	Set<MatchingRelation> inf = new HashSet<MatchingRelation>();
	double moy = 0;
	for (MatchingRelation mr : s) {
	    moy += mr.getStrength();
	}
	moy = moy / s.size();

	double supT = moy;
	double infT = seuil;

	int oldSize = -1;
	while (oldSize != inf.size()) {
	    oldSize = inf.size();
	    double moyInf = 0;
	    double moySup = 0;
	    for (MatchingRelation mr : s) {
		double val = mr.getStrength();
		if (Math.abs(val - infT) < Math.abs(supT - val)) {
		    inf.add(mr);
		    moyInf += mr.getStrength();
		} else {
		    inf.remove(mr);
		    moySup += mr.getStrength();
		}
	    }
	    infT = moyInf / inf.size();
	    supT = moySup / (s.size() - inf.size());
	}

	// s.removeAll(inf);
	for (MatchingRelation mr : inf) {

	    try {
		System.err.println(mr.getObject1AsURI() + " - "
			+ mr.getObject2AsURI());
	    } catch (AlignmentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}
    }

}
