/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   AEntity.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.ontology.entities;

import java.util.Map;
import java.util.Set;

import fr.inrialpes.exmo.ontosim.entity.model.Entity;
import fr.inrialpes.exmo.ontowrap.Annotation;
import fr.inrialpes.exmo.ontowrap.OntowrapException;

public interface AEntity extends Entity {

    public Set<String> getTerms();

    public int getNbCommonTerms(AEntity e);

    public int getNbTerms();

    public void addTerms(Set<String> s);

    public void addTerm(String s);

    public void addTerms(AEntity e);

    public boolean containTerm(String s);

    public void retainTerms(AEntity e);

    public Set<AEntity> getSubEntities(boolean direct);

    public Set<AEntity> getSuperEntities(boolean direct);

    public boolean isDescendantOf(AEntity e);

    public boolean isAncestorOf(AEntity e);

    public boolean addParent(AEntity e);

    public boolean addChild(AEntity e);

    public AEntity getRoot();
    
    public void freeTerms();
    
    // to add in ontosim EntityInterface
    public Set<Annotation> getAnnotationsL() throws OntowrapException;

}
