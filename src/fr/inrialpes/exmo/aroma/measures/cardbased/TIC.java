/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   TIC.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.measures.cardbased;

public class TIC extends TI {

    public TIC() {
	super();
	// TODO Auto-generated constructor stub
    }

    public double getIndice(int na, int nb, int nab, int n) {

	double TI1 = super.getIndice(na, nb, nab, n);
	double TI2 = super.getIndice(n - nb, n - na, n - na - nb + nab, n);

	if (TI1 >= 0 && TI2 >= 0)
	    return TI1 * TI2;
	return 0;
    }

}
