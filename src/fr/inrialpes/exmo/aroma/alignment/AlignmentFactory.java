/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   AlignmentFactory.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.alignment;

import org.semanticweb.owl.align.AlignmentException;

import fr.inrialpes.exmo.aroma.measures.cardbased.Index;
import fr.inrialpes.exmo.aroma.ontology.entities.AEntity;

public class AlignmentFactory {

    private AEntity hierarchy1;

    private AEntity hierarchy2;

    private Index index;

    public AlignmentFactory(AEntity h1, AEntity h2, Index index) {
	hierarchy1 = h1;
	hierarchy2 = h2;
	this.index = index;
    }

    public Index getIndex() {
	return index;
    }

    public boolean isH1(AEntity h1) {
	return h1 == hierarchy1;
    }

    public Implication getImplication(AEntity src, AEntity dst) {
	double value = index.getIndice(src.getNbTerms(), dst.getNbTerms(), src
		.getNbCommonTerms(dst), hierarchy1.getNbTerms());
	try {
	    if (src.getRoot() == hierarchy1 && dst.getRoot() == hierarchy2)
		return new Implication(src, dst, value, true);
	    else if (src.getRoot() == hierarchy2 && dst.getRoot() == hierarchy1)
		return new Implication(dst, src, value, false);
	} catch (AlignmentException e) {
	    e.printStackTrace();
	}
	return null;
    }

    public Equivalence getEquivalence(AEntity src, AEntity dst) {
	double value = Math.sqrt(index.getIndice(src.getNbTerms(), dst.getNbTerms(), src.getNbCommonTerms(dst), hierarchy1.getNbTerms())
		* index.getIndice(dst.getNbTerms(), src.getNbTerms(), src.getNbCommonTerms(dst), hierarchy1.getNbTerms()));
	try {
	    if (src.getRoot() == hierarchy1 && dst.getRoot() == hierarchy2)
		return new Equivalence(src, dst, value);
	    else if (src.getRoot() == hierarchy2 && dst.getRoot() == hierarchy1)
		return new Equivalence(dst, src, value);
	} catch (AlignmentException e) {
	    e.printStackTrace();
	}
	return null;
    }

    public Equivalence getEquivalence(Implication imp1, Implication imp2) {
	if (imp1.getSource().equals(imp2.getDest())
		&& imp1.getDest().equals(imp2.getSource())) {
	    try {
		return new Equivalence(imp1.getObject1(), imp1.getObject2(),
			Math.sqrt(imp1.getStrength() * imp2.getStrength()));
	    } catch (AlignmentException e) {
		e.printStackTrace();
	    }
	}
	return null;
    }

}
