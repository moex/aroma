/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   AEntityImpl.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.ontology.entities;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import fr.inrialpes.exmo.ontosim.entity.model.EntityImpl;
import fr.inrialpes.exmo.ontowrap.Annotation;
import fr.inrialpes.exmo.ontowrap.LoadedOntology;
import fr.inrialpes.exmo.ontowrap.OntowrapException;

public class AEntityImpl extends EntityImpl implements AEntity {

    protected Set<AEntity> parents;

    protected Set<AEntity> children;

    private Set<AEntity> ascendants;

    private Set<AEntity> descendants;

    private Set<String> terms;

    public AEntityImpl(LoadedOntology<?> ont, Object e) {
	super(ont, e);
	terms = new HashSet<String>();
	parents = new HashSet<AEntity>();
	children = new HashSet<AEntity>();

	ascendants = new HashSet<AEntity>();
	descendants = new HashSet<AEntity>();
    }

    public Set<String> getTerms() {
	return Collections.unmodifiableSet(terms);
    }

    public int getNbCommonTerms(AEntity e) {
	int nb = 0;
	for (String term : terms) {
	    if (e.containTerm(term))
		nb++;
	}
	return nb;
    }

    public int getNbTerms() {
	return terms.size();
    }

    public boolean containTerm(String s) {
	return terms.contains(s);
    }

    public void addTerm(String t) {
	terms.add(t);
	for (AEntity parent : getSuperEntities(true))
	    parent.addTerm(t);

    }

    public void addTerms(Set<String> s) {
	terms.addAll(s);
	for (AEntity parent : getSuperEntities(true))
	    parent.addTerms(s);

    }

    public void retainTerms(AEntity e) {
	terms.retainAll(e.getTerms());
	for (AEntity child : children) {
	    child.retainTerms(e);
	}
    }

    public Set<AEntity> getSubEntities(boolean direct) {
	if (direct)
	    return Collections.unmodifiableSet(children);
	// if (descendants != null)
	return descendants;
	/*
	 * SoftReference<HashSet<AEntity>> sr = new
	 * SoftReference<HashSet<AEntity>>( new HashSet<AEntity>()); descendants
	 * = sr.get(); descendants.addAll(children); for (AEntity child :
	 * children) descendants.addAll(child.getSubEntities(false)); return
	 * Collections.unmodifiableSet(descendants);
	 */
    }

    public Set<AEntity> getSuperEntities(boolean direct) {
	if (direct)
	    return Collections.unmodifiableSet(parents);
	// if (ascendants != null)
	return ascendants;
	/*
	 * SoftReference<HashSet<AEntity>> sr = new
	 * SoftReference<HashSet<AEntity>>( new HashSet<AEntity>()); ascendants
	 * = sr.get(); ascendants.addAll(parents); for (AEntity parent :
	 * parents) ascendants.addAll(parent.getSuperEntities(false)); return
	 * Collections.unmodifiableSet(ascendants);
	 */
    }

    public boolean isAncestorOf(AEntity e) {
	return equals(e) || descendants.contains(e);
	/*
	 * if (equals(e) || children.contains(e)) return true; for (AEntity p :
	 * children) { if (p.isAncestorOf(e)) return true; } return false;
	 */
    }

    public boolean isDescendantOf(AEntity e) {
	return equals(e) || ascendants.contains(e);
	/*
	 * if (equals(e) || parents.contains(e)) return true; for (AEntity p :
	 * parents) { if (p.isDescendantOf(e)) return true; } return false;
	 */
    }

    public boolean addParent(AEntity e) {
	if (isAncestorOf(e))
	    return false;
	if (parents.add(e)) {
	    e.addTerms(this);
	    // this.ascendants = null;
	    ascendants.add(e);
	    ascendants.addAll(e.getSuperEntities(false));
	    for (AEntity d : descendants) {
		d.getSuperEntities(false).add(e);
		d.getSuperEntities(false).addAll(e.getSuperEntities(false));
	    }
	    e.addChild(this);
	    return true;
	}
	return false;
    }

    public boolean addChild(AEntity e) {
	if (isDescendantOf(e))
	    return false;
	if (children.add(e)) {
	    // addTerms(e);
	    // this.descendants = null;
	    descendants.add(e);
	    descendants.addAll(e.getSubEntities(false));
	    for (AEntity a : ascendants) {
		a.getSubEntities(false).add(e);
		a.getSubEntities(false).addAll(e.getSubEntities(false));
	    }
	    e.addParent(this);
	    return true;
	}
	return false;
    }

    public AEntity getRoot() {
	if (parents.size() > 0)
	    return parents.iterator().next().getRoot();
	return this;
    }

    public void addTerms(AEntity e) {
	addTerms(e.getTerms());
    }

    public void freeTerms() {
	terms=null;
    }


    public Set<Annotation> getAnnotationsL() throws OntowrapException {
	return this.getOntology().getEntityAnnotationsL(this.getObject());
    }

}
