/**
 *   Copyright 2007, 2011 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   PostAlignment.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import fr.inrialpes.exmo.aroma.alignment.AlignmentFactory;
import fr.inrialpes.exmo.aroma.alignment.Equivalence;
import fr.inrialpes.exmo.aroma.alignment.Implication;
import fr.inrialpes.exmo.aroma.alignment.MatchingRelation;
import fr.inrialpes.exmo.aroma.measures.entitybased.AnnotMaxSharedTerms;
import fr.inrialpes.exmo.aroma.ontology.entities.AEntity;
import fr.inrialpes.exmo.aroma.utils.ling.StemmedTermExtractor;
import fr.inrialpes.exmo.aroma.utils.ling.StringTransform;
import fr.inrialpes.exmo.ontosim.Measure;
import fr.inrialpes.exmo.ontosim.entity.model.Entity;
import fr.inrialpes.exmo.ontosim.util.HungarianAlgorithm;
import fr.inrialpes.exmo.ontosim.util.matrix.Matrix;
import fr.inrialpes.exmo.ontosim.util.matrix.MatrixDoubleArray;
import fr.inrialpes.exmo.ontosim.util.matrix.MatrixFile;
import fr.inrialpes.exmo.ontowrap.Annotation;

public class PostAlignment {

    /**
     * built a map between each object and its correspondent entities according
     * the implication relation
     * 
     * @param s
     * @return
     */
    public static Map<AEntity, Set<AEntity>> ranges(Set<MatchingRelation> s) {
	Map<AEntity, Set<AEntity>> ranges = new HashMap<AEntity, Set<AEntity>>();
	for (MatchingRelation mr : s) {
	    if (mr instanceof Equivalence
		    || (mr instanceof Implication && ((Implication) mr).getSource().equals(mr.getObject1())))
		addInMap(ranges, mr.getObject1(), mr.getObject2());
	    if (mr instanceof Equivalence
		    || (mr instanceof Implication && ((Implication) mr).getSource().equals(mr.getObject2())))
		addInMap(ranges, mr.getObject2(), mr.getObject1());
	}
	return ranges;
    }

    private static void addInMap(final Map<AEntity, Set<AEntity>> map, final AEntity e1, final AEntity e2) {
	Set<AEntity> s = map.get(e1);
	if (s == null) {
	    s = new HashSet<AEntity>();
	    map.put(e1, s);
	}
	s.add(e2);
    }

    public static Set<AEntity> ppag(AEntity x, AEntity h1, AEntity h2, Map<AEntity, Set<AEntity>> ranges, boolean direct) {
	Set<AEntity> ppagS = new HashSet<AEntity>();
	Set<AEntity> fathers = x.getSuperEntities(true);

	if (fathers.size() == 0) {
	    /*if (x==h1) ppagS.add(h2);
	    else ppagS.add(h1);*/
	    return ppagS;
	}

	for (AEntity aFather : fathers) {
	    if (ranges.containsKey(aFather)) {
		ppagS.addAll(ranges.get(aFather));
	    }
	}
	if (ppagS.size() == 0 && !direct) {
	    for (AEntity aFather : fathers) {
		ppagS.addAll(ppag(aFather, h1, h2, ranges, direct));
	    }
	}
	return ppagS;
    }


    public static Set<AEntity> getNotMatched(AEntity h, boolean direct,Set<MatchingRelation> s) {
	Set<AEntity> notAligned = new HashSet<AEntity>(h.getSubEntities(direct));
	if (s.size() == 0)
	    return notAligned;// Collections.emptySet();
	boolean h1 = s.iterator().next().getObject1().getRoot() == h.getRoot();
	for (MatchingRelation mr : s) {
	    AEntity ent;
	    if (h1)
		ent = mr.getObject1();
	    else
		ent = mr.getObject2();
	    notAligned.remove(ent);
	}
	return notAligned;
    }

    public static Set<AEntity> children(Set<AEntity> entities) {
	Set<AEntity> children = new HashSet<AEntity>();
	for (AEntity ent : entities) {
	    children.addAll(ent.getSubEntities(true));
	}
	return children;
    }

    public static Set<AEntity> getMatched(Set<MatchingRelation> s) {
	Set<AEntity> matched = new HashSet<AEntity>();
	for (MatchingRelation mr : s) {
	    matched.add(mr.getObject1());
	    matched.add(mr.getObject2());
	}
	return matched;
    }



    public static void alignSyntax(AEntity h1, AEntity h2, Set<MatchingRelation> s, AlignmentFactory af, Measure<Entity<?>> lexicalM, double threshold) {
	Matrix<AEntity, AEntity> values = new MatrixFile<AEntity, AEntity>();//new MatrixDouble<AEntity, AEntity>();//

	//Measure<Entity<?>> lexicalM = new MeasureCache<Entity<?>>(new EntityLexicalMeasure());//new EntityLexicalMeasure());//new LexicalAEntityM();//new MeasureCache<Entity<?>>(new LexicalTokenMeasure());// (new EntityLexicalMeasure()); 
	Set<AEntity> notAlignedh1 = null;
	Set<AEntity> notAlignedh2 = null;

	notAlignedh1 = getNotMatched(h1, false, s);
	notAlignedh2 = getNotMatched(h2, false, s);
	notAlignedh1.addAll(notAlignedh2);

	Map<AEntity, Set<AEntity>> ranges = ranges(s);
	
	boolean findCandidate=false;
	boolean allTheSame=true;
	double oldVal=Double.NaN;
	
	
	for (AEntity x : notAlignedh1) {
	    Set<AEntity> candidates = ppag(x, h1, h2, ranges, false);
	    boolean underThreshold = true;
	    while (underThreshold && candidates.size() > 0) {
		candidates = children(candidates);
		//candidates.retainAll(notAlignedh2);
		candidates.retainAll(notAlignedh1);
		for (AEntity ent : candidates) {
		    double sim = lexicalM.getSim(x, ent);
    		    if (sim > threshold) {
    			if (sim!=oldVal && oldVal!=Double.NaN) {
    			    allTheSame=false;
    			}
    			oldVal=sim;
    			values.put(x, ent, sim);
    			underThreshold = false;
    			findCandidate=true;
    		    }
		}
	    }
	}

	if (!findCandidate || allTheSame) return;
	//lexicalM = null;
	MatrixDoubleArray<AEntity, AEntity> mda;
	if (values.getDimR().size() > values.getDimC().size())
	    mda = values.toArrayT();
	else
	    mda = values.toArray();
	values=null;
	int[][] res = HungarianAlgorithm.hgAlgorithm(mda.getValues(), "max");
	for (int i = 0; i < res.length; i++) {
	    Equivalence eq = af.getEquivalence(mda.getRows().get(res[i][0]),
		    mda.getCols().get(res[i][1]));
	    if (mda.getValues()[res[i][0]][res[i][1]] > threshold) {// && eq.getStrength ()>0 ) 
		eq.setExtension("aroma", "method", lexicalM.getClass().getCanonicalName());
		s.add(eq);
	    }
	}

    }
    
    public static void eqLabelAlign(AEntity h1, AEntity h2, Set<MatchingRelation> s, AlignmentFactory af) {
 
	Set<AEntity> notAlignedh1 = null;
	Set<AEntity> notAlignedh2 = null;

	notAlignedh1 = getNotMatched(h1, false, s);
	notAlignedh2 = getNotMatched(h2, false, s);
	
	Set<AEntity> min = notAlignedh1;
	Set<AEntity> max = notAlignedh2;
	if (notAlignedh1.size()>notAlignedh2.size()) {
	    min = notAlignedh2;
	    max = notAlignedh1;
	}
	
	Measure<Entity<?>> m = new AnnotMaxSharedTerms();
	for (AEntity x : max) {
	    for (AEntity y : min) {
		double sim = m.getSim(x, y);
		    if (sim==1) {
			Equivalence eq = af.getEquivalence(x,y);
			s.add(eq);
			//eq.setExtension("aroma", "method", m.getClass().getCanonicalName());
		    }
	    }
	}
    }
   
    
    
    public static void eqLabelAlign2(AEntity h1, AEntity h2, Set<MatchingRelation> s, int threshold, AlignmentFactory af) {
	Set<AEntity> notAlignedh1 = getNotMatched(h1,false,s);
	Set<AEntity> notAlignedh2 = getNotMatched(h2,false,s);
	eqLabelAlign2(notAlignedh1,notAlignedh2,s,threshold, af);
    }

    
    public static void eqLabelAlign2(Set<AEntity> notAlignedh1, Set<AEntity> notAlignedh2, Set<MatchingRelation> s, int threshold, AlignmentFactory af) {
	StemmedTermExtractor te = new StemmedTermExtractor();

	
	Map<Annotation,Set<AEntity>> lbl2Ent = new HashMap<Annotation,Set<AEntity>>();
	Map<AEntity,Integer>  lblCounts = new HashMap<AEntity,Integer>();
	
	for (AEntity e : notAlignedh1) {
	    try {
		Set<Annotation> annots = e.getAnnotationsL();
	
		String localName = e.getURI().getFragment();
		if (localName!=null)
		    annots.add(new Annotation(StringTransform.addSpaces(localName)));
		for (Annotation a : annots) {
		    Annotation annot;
		    if (a.getLanguage()==null)
			annot=new Annotation(te.getStemmedText(a.getValue(),a.getLanguage()));
		    else
			annot = new Annotation(te.getStemmedText(a.getValue(),a.getLanguage()),a.getLanguage());
		    Set<AEntity> entities = lbl2Ent.get(annot);
		    if (entities==null) {
			entities = new HashSet<AEntity>();
			lbl2Ent.put(annot, entities);
		    }
		    entities.add(e);
		}
	    }
	    catch (Exception ex) {ex.printStackTrace();}
	}

	Map<AEntity,Integer> counts = new HashMap<AEntity,Integer>();
	
	for (AEntity e : notAlignedh2) {
	    try {
		Set<Annotation> annots = e.getAnnotationsL();
		String localName = e.getURI().getFragment();
		if (localName!=null)
		    annots.add(new Annotation(StringTransform.addSpaces(localName)));
		// local name has not alwaays a meaning -> we do not count it in the annot size
		//int e1S = annots.size()-1;
		
		int max=0;
		ArrayList<AEntity> maxE=new ArrayList<AEntity>();
		
		for (Annotation a : annots) {
		    Annotation at;
		    if (a.getLanguage()==null)
			at= new Annotation(te.getStemmedText(a.getValue(),a.getLanguage()));
		    else
			at= new Annotation(te.getStemmedText(a.getValue(),a.getLanguage()),a.getLanguage());
			
		    Set<AEntity> entities = lbl2Ent.get(at);
		    
		    if (entities!=null) {
			for (AEntity e2 : entities) {
			    Integer c = counts.get(e2);
			    int v = 0;
			    if (c!=null) 
				v = c.intValue();
			    v++;
			    counts.put(e2,v);
			    
			    if (v>threshold) {
				MatchingRelation rel = af.getEquivalence(e, e2);
				//rel.setStrength(2.0);
				s.add(rel);
			    }
			    
			    if (v>max) {
				max=v;
				maxE.clear();
			    }
			    if (v==max) {
				maxE.add(e2);
			    }
			}
		    }
		}
		for (AEntity e2 : maxE) {
		    //int realThreshold=Math.min(threshold,Math.min(e1S, lblCounts.get(e2)));
		    if (max>=threshold) {
			MatchingRelation rel = af.getEquivalence(e, e2);
			//rel.setStrength(2.0);
			s.add(rel);
		    }
		}
	    }
	    catch (Exception ex) {ex.printStackTrace();}
	    counts.clear();
	}
	te.free();
	
    }
    
    public static Map<Annotation,AEntity> indexAnnots(Set<AEntity> entities, StemmedTermExtractor te) {
	Map<Annotation,AEntity> lbl2Ent = new HashMap<Annotation,AEntity>();	
	Set<Annotation> toRemove = new HashSet<Annotation>();
	
	for (AEntity e : entities) {
	    try {
		Set<Annotation> annots = e.getAnnotationsL();
	
		String localName = e.getURI().getFragment();
		if (localName!=null)
		    annots.add(new Annotation(StringTransform.addSpaces(localName)));
		
		for (Annotation a : annots) {
		    Annotation annot;
		    if (a.getLanguage()==null) 
			annot=new Annotation(te.getStemmedText(a.getValue(),a.getLanguage()));
		    else
			annot=new Annotation(te.getStemmedText(a.getValue(),a.getLanguage()),a.getLanguage());
		    if (lbl2Ent.put(annot, e)!=null) {
			toRemove.add(annot);
		    }
		}
	    }
	    catch (Exception ex) {ex.printStackTrace();}
	}
	lbl2Ent.keySet().removeAll(toRemove);
	return lbl2Ent;
    }
    
    
    public static void eqLabelAlign3(Set<AEntity> notAlignedh1, Set<AEntity> notAlignedh2, Set<MatchingRelation> s, int threshold, AlignmentFactory af) {
	StemmedTermExtractor te = new StemmedTermExtractor();

	 Map<Annotation,AEntity> h1Idx = indexAnnots(notAlignedh1,te);
	 Map<Annotation,AEntity> h2Idx = indexAnnots(notAlignedh2,te);
	
	 h1Idx.keySet().retainAll(h2Idx.keySet());
	 h2Idx.keySet().retainAll(h1Idx.keySet());
	 
	Map<AEntity,Map<AEntity,Set<String>>> counts = new HashMap<AEntity,Map<AEntity,Set<String>>>();
	
	for (Map.Entry<Annotation,AEntity> a1 : h1Idx.entrySet()) {
	    Map<AEntity,Set<String>> m = counts.get(a1.getValue());
	    if (m==null) {
		m = new HashMap<AEntity,Set<String>>();
		counts.put(a1.getValue(), m);
	    }
	    AEntity e2 = h2Idx.get(a1.getKey());
	    
	    Set<String> languages=m.get(e2);
	    //int v=0;
	    if (languages==null) {//(m.containsKey(e2))
		//v = m.get(e2).size();
		languages=new HashSet<String>();
		m.put(e2, languages);
	    }
	    
	    if (languages!=Collections.<String>emptySet()) {
        	    //v++;
		languages.add(a1.getKey().getLanguage());
        	if (languages.size()>threshold) {
        	    MatchingRelation rel = af.getEquivalence(a1.getValue(), e2);
        	    s.add(rel);
        	    m.put(e2,Collections.<String>emptySet() );
        	}
	    }
	}
    }
    
    public static void eqLabelAlign3(AEntity h1, AEntity h2, Set<MatchingRelation> s, int threshold, AlignmentFactory af) {
	Set<AEntity> notAlignedh1 = getNotMatched(h1,false,s);
	Set<AEntity> notAlignedh2 = getNotMatched(h2,false,s);
	eqLabelAlign3(notAlignedh1,notAlignedh2,s,threshold, af);
    }
    
    public static SortedMap<String,AEntity> indexAnnotsWL(Set<AEntity> entities, StemmedTermExtractor te) {
	SortedMap<String,AEntity> lbl2Ent = new TreeMap<String,AEntity>();	
	Set<String> toRemove = new HashSet<String>();
	
	for (AEntity e : entities) {
	    try {
		Set<Annotation> annots = e.getAnnotationsL();
	
		String localName = e.getURI().getFragment();
		if (localName!=null)
		    annots.add(new Annotation(StringTransform.addSpaces(localName)));
		
		for (Annotation a : annots) {
		    String s = te.getStemmedText(a.getValue(),a.getLanguage());
		    int idxS=0;
		    while (idxS!=-1) {
			idxS=s.indexOf(' ');
			String toAdd = a.getLanguage()+"_"+s;
    		    	if (lbl2Ent.put(toAdd, e)!=null)
    		    	    toRemove.add(toAdd);
    		    	s=s.substring(idxS+1);
		    }
		}
	    }
	    catch (Exception ex) {ex.printStackTrace();}
	}
	lbl2Ent.keySet().removeAll(toRemove);
	return lbl2Ent;
    }
    
    public static void incLabelAlign(Set<AEntity> notAlignedh1, Set<AEntity> notAlignedh2, Set<MatchingRelation> s, int threshold, AlignmentFactory af) {
	StemmedTermExtractor te = new StemmedTermExtractor();

	SortedMap<String,AEntity> h1Idx = indexAnnotsWL(notAlignedh1,te);
	Map<Annotation,AEntity> h2Idx = indexAnnots(notAlignedh2,te);
	 
	Map<AEntity,Map<AEntity,Set<String>>> counts = new HashMap<AEntity,Map<AEntity,Set<String>>>();
	
	for (Map.Entry<Annotation,AEntity> a2 : h2Idx.entrySet()) {
	    String annot = a2.getKey().getLanguage()+"_"+a2.getKey().getValue();
	    SortedMap<String,AEntity> tail = h1Idx.tailMap(annot);
	    for (Map.Entry<String,AEntity> a1 : tail.entrySet()) {
		if (!(a1.getKey().startsWith(annot+" ") || a1.getKey().equals(annot)))
		    break;
		    
		Map<AEntity,Set<String>> m = counts.get(a2.getValue());
		if (m==null) {
		    m = new HashMap<AEntity,Set<String>>();
		    counts.put(a2.getValue(), m);
		}
		
		Set<String> languages=m.get(a1.getValue());
		if (languages==null) {
			languages=new HashSet<String>();
			m.put(a1.getValue(), languages);
		}
		    
		if (languages!=Collections.<String>emptySet()) {
		    languages.add(a2.getKey().getLanguage());
		    if (languages.size()>threshold) {
        		 MatchingRelation rel = af.getImplication(a1.getValue(), a2.getValue());
        	         //System.err.println(a1.getValue().getURI()+" -> "+a2.getValue().getURI());
        		 rel.setStrength(2.0);
        		 s.add(rel);
        		 m.put(a1.getValue(), Collections.<String>emptySet());
		    }
		}
	    }
	    
	}
    }
    
    public static void incLabelAlign(AEntity h1, AEntity h2, Set<MatchingRelation> s, int threshold, AlignmentFactory af) {
	Set<AEntity> notAlignedh1 = getNotMatched(h1,false,s);
	Set<AEntity> notAlignedh2 =getNotMatched(h2,false,s);// h2.getSubEntities(false);//
	incLabelAlign(notAlignedh1,notAlignedh2,s,threshold, af);
    }
    
    
    
    
    
    public static int getLayers(AEntity h1, Map<Integer,Set<AEntity>> layers) {
	Set<AEntity> sub = h1.getSubEntities(true);
	int max=-1;
	if (sub!=null && sub.size()>0)
        	for (AEntity e : sub) {
        	    int d = getLayers(e,layers);
        	    if (d>max) max=d;
        	}
	Set<AEntity> currentL = layers.get(max+1);
	if (currentL==null) {
	    currentL = new HashSet<AEntity>();
	    layers.put(max+1, currentL);
	}
	currentL.add(h1);
	return max+1;
    }
    
    public static void addToMatchCount(HashMap<AEntity, HashMap<AEntity, Integer>> counts, Set<AEntity> s1, Set<AEntity> s2) {
	for (AEntity e1 : s1)
	    for (AEntity e2 : s2) {
		HashMap<AEntity, Integer> ec = counts.get(e1);
		if (ec == null) {
		    ec = new HashMap<AEntity, Integer>();
		    counts.put(e1, ec);
		}
		Integer c = ec.get(e2);
		if (c == null)
		    ec.put(e2, 1);
		else
		    ec.put(e2, c.intValue() + 1);
	    }
    }
    
    public static void alignParentWhenChildrenMatch(AEntity h1, AEntity h2, Set<MatchingRelation> s, AlignmentFactory af) {
	
	HashMap<AEntity,HashMap<AEntity,Integer>> counts = new HashMap<AEntity,HashMap<AEntity,Integer>>();
	for (MatchingRelation mr : s) {
	    Set<AEntity> s1 = mr.getObject1().getSuperEntities(true);
	    Set<AEntity> s2 = mr.getObject2().getSuperEntities(true);
	    addToMatchCount(counts,s1,s2);
	    
	}
	
	Set<AEntity> notMatchedH1 = getNotMatched(h1,true,s);
	Set<AEntity> notMatchedH2 = getNotMatched(h2,true,s);
	
	Map<Integer,Set<AEntity>> layersh1 = new HashMap<Integer,Set<AEntity>>();
	int depthH1 = getLayers(h1,layersh1);
	
	Map<Integer,Set<AEntity>> layersh2 = new HashMap<Integer,Set<AEntity>>();
	int depthH2 = getLayers(h2,layersh2);
	
	int maxDepth = Math.max(depthH1,depthH2);
	
	Set<AEntity> currentH1=new HashSet<AEntity>();
	Set<AEntity> currentH2=new HashSet<AEntity>();
	
	for (int i=1 ; i<maxDepth ; i++) {
	    Set<AEntity> lh1 = layersh1.get(i);
	    if (lh1!=null) {
		lh1.retainAll(notMatchedH1);
		currentH1.addAll(lh1);
	    }
	    
	    Set<AEntity> lh2 = layersh2.get(i);
	    if (lh2!=null) {
		lh2.retainAll(notMatchedH2);
		currentH2.addAll(lh2);
	    }
	    
	    Iterator<AEntity> ith1 = currentH1.iterator();
	    while (ith1.hasNext()) {
		AEntity e1 = ith1.next();
		 Iterator<AEntity> ith2 = currentH2.iterator();
		 while (ith2.hasNext()) {
		     AEntity e2 = ith2.next();
		     int c = 0;
		     if (counts.containsKey(e1) && counts.get(e1).containsKey(e2)) c = counts.get(e1).get(e2);
		     if (c>1) {
			 s.add(af.getEquivalence(e1, e2));
			 addToMatchCount(counts,e1.getSuperEntities(true),e2.getSuperEntities(true));
			 ith1.remove();
			 ith2.remove();
			 break;
			 
		     }
		 }
	    }
	}
    }
    
    public static AEntity getObject(MatchingRelation mr, boolean object1) {
	if (object1)
	    return mr.getObject1();
	else
	    return mr.getObject2();
    }   
    
    public static void closure(Set<MatchingRelation> s, AlignmentFactory af) {
	HashSet<MatchingRelation> added = new HashSet<MatchingRelation>();
	for (MatchingRelation mr :s) {
	    Set<AEntity> notMatch = getNotMatched(mr.getObject1(),true,s); 
	    for (AEntity e : notMatch) {
		added.add(af.getImplication(e, mr.getObject2()));
	    }
	    notMatch = getNotMatched(mr.getObject2(),true,s); 
	    for (AEntity e : notMatch) {
		added.add(af.getImplication(e, mr.getObject1()));
	    }
	}
	s.addAll(added);
    }
}
