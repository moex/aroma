/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   IntensiteImplicationHyperGeo.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.measures.cardbased;

import org.apache.commons.math.MathException;
import org.apache.commons.math.distribution.HypergeometricDistribution;
import org.apache.commons.math.distribution.HypergeometricDistributionImpl;

public class IntensiteImplicationHyperGeo extends Index {

    public IntensiteImplicationHyperGeo() {
	super();
    }

    @Override
    public double getIndice(int na, int nb, int nab, int n) {
	// HypergeometricDistribution distri = new
	// HypergeometricDistributionImpl(n,n-nb,na);
	HypergeometricDistribution distri = new HypergeometricDistributionImpl(
		n, na, n - nb);
	try {
	    return 1.0 - distri.cumulativeProbability(na - nab);
	} catch (MathException e) {
	    // System.err.println("balbla");
	    return Double.NaN;
	}
    }

}
