/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   MatchingRelation.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.alignment;

import java.net.URI;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.Relation;

import fr.inrialpes.exmo.align.impl.BasicCell;
import fr.inrialpes.exmo.aroma.ontology.entities.AEntity;

public abstract class MatchingRelation extends BasicCell {

    protected AEntity object1;

    protected AEntity object2;

    public MatchingRelation(AEntity ent1, AEntity ent2, Relation rel,
	    double value) throws AlignmentException {
	super(null, ent1, ent2, rel, value);
	object1 = ent1;
	object2 = ent2;
    }

    public AEntity getObject1() {
	return object1;
    }

    public URI getObject1AsURI(Alignment al) throws AlignmentException {
	return object1.getURI();
    }

    public AEntity getObject2() {
	return object2;
    }

    public URI getObject2AsURI(Alignment al) throws AlignmentException {
	return object2.getURI();
    }

    public boolean equals(Object o) {
	if (o instanceof MatchingRelation) {
	    MatchingRelation mr = (MatchingRelation) o;
	    return mr.getObject1().equals(object1)
		    && mr.getObject2().equals(object2)
		    && mr.getRelation().equals(relation);
	}
	return super.equals(o);
    }

    public int hashCode() {
	return getRelation().getClass().hashCode() + object1.hashCode()
		+ object2.hashCode();
    }
    
    public abstract boolean isGenerativeOf(MatchingRelation rel);
}
