/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   LexicalTokenMeasure.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.measures.entitybased;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.StopAnalyzer;
import org.apache.lucene.analysis.Token;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.snowball.SnowballAnalyzer;
import org.apache.lucene.analysis.tokenattributes.TermAttribute;
import org.apache.lucene.util.Version;

import fr.inrialpes.exmo.ontosim.Measure;
import fr.inrialpes.exmo.ontosim.entity.model.Entity;


/**
 * Compare two entities and returns the maximum proportion of shared terms between the sets of annotations
 * @author jerome DAVID
 * 
 */
public class AnnotMaxSharedTerms implements Measure<Entity<?>> {

    private Analyzer analyzer = new SnowballAnalyzer(Version.LUCENE_30, "English",StopAnalyzer.ENGLISH_STOP_WORDS_SET);


    private Map<Entity<?>, Set<Set<String>>> map = new HashMap<Entity<?>, Set<Set<String>>>();


    public double getDissim(Entity<?> o1, Entity<?> o2) {
	return 1 - getMeasureValue(o1, o2);
    }


    public fr.inrialpes.exmo.ontosim.Measure.TYPES getMType() {
	return TYPES.similarity;
    }

    private void analyseString(String a, Set<String> s) {
	if (a == null)
	    return;
	try {
	    TokenStream ts = analyzer.reusableTokenStream("", new StringReader(a));
	    TermAttribute termAtt = ts.addAttribute(TermAttribute.class);

	    while (ts.incrementToken()) {

		s.add(termAtt.term());
	    }
	} catch (IOException ex) {
	    ex.printStackTrace();
	}
    }
    
    private void extractTerms(Entity<?> e) {
	HashSet<Set<String>> s = new HashSet<Set<String>>();
	
	for (String a : e.getAnnotations(null)) {
	    Set<String> x = new HashSet<String>();
	    analyseString(a,x);
	    s.add(x);
	}
	Set<String> x = new HashSet<String>();
	analyseString(e.getURI().getFragment(), x);
	s.add(x);
	map.put(e, s);
    }


    public double getMeasureValue(Entity<?> o1, Entity<?> o2) {
	if (!map.containsKey(o1))
	    extractTerms(o1);
	if (!map.containsKey(o2))
	    extractTerms(o2);
	
	Set<Set<String>> s1 = map.get(o1);
	Set<Set<String>> s2 = map.get(o2);
	
	if (Math.min(s1.size(), s2.size()) == 0)
	    return 0;

	double nbEq=0;
	double max=0;
	
	for (Set<String> a : s1) {
	    for (Set<String> b : s2) {
		int i=0;
		if (a.size()==0 || b.size()==0) continue;
		for (String as : a) if (b.contains(as)) i++;
		if (i==Math.max(a.size(), b.size())) nbEq++;
	    }
	}
	int mins1s2 = Math.min(s1.size(), s2.size());
	if (mins1s2<nbEq) return 1.;
	return nbEq/mins1s2;
}


    public double getSim(Entity<?> o1, Entity<?> o2) {
	return getMeasureValue(o1, o2);
    }

}
