/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   JENAAEntityFactory.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.ontology.entities;

import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.vocabulary.OWL;

import fr.inrialpes.exmo.ontowrap.Annotation;
import fr.inrialpes.exmo.ontowrap.HeavyLoadedOntology;
import fr.inrialpes.exmo.ontowrap.LoadedOntology;
import fr.inrialpes.exmo.ontowrap.OntowrapException;

public class JENAAEntityFactory extends AbstractAEntityFactory {

    public JENAAEntityFactory(boolean useCache) {
	super(useCache);
    }
    
    public JENAAEntityFactory(boolean useCache, Set<URI> forbiddenURIs) {
	super(useCache,forbiddenURIs);
    }

    @Override
    protected AEntityImpl createARoot(LoadedOntology<?> onto) {
	return new AEntityImpl(onto, OWL.Thing);
    }

    private void getIndividualTerms(Individual ind, Set<Annotation> terms) {
	if (ind.getURI() != null) {
	    if (URI.create(ind.getURI()).getFragment()!=null)
	    	terms.add(new Annotation(URI.create(ind.getURI()).getFragment()));
	    else
		terms.add(new Annotation(ind.getURI()));
		
	    Iterator<?> j = ind.listProperties();
	    while (j.hasNext()) {
		Statement st = (Statement) j.next();
		if (st.getObject().isLiteral()) {
		    Node n = st.getObject().asNode();
		    terms.add(new Annotation(n.getLiteralValue().toString(),n.getLiteralLanguage()));
		}
	    }
	}
	// return terms;
    }

    protected  Set<Annotation> getClassSpecificTerms(Object aClass, HeavyLoadedOntology<?> onto) {
	Set<Annotation> terms = new HashSet<Annotation>();
	OntClass classe = (OntClass) aClass;
	Iterator<?> i = classe.listInstances(true);
	while (i.hasNext()) {
	    Individual ind = (Individual) i.next();
	    getIndividualTerms(ind, terms);
	}
	return terms;
    }

    protected  Set<Annotation> getDataPSpecificTerms(Object aClass, LoadedOntology<?> onto) {
	Set<Annotation> terms = new HashSet<Annotation>();
	DatatypeProperty dp = (DatatypeProperty) aClass;

	Iterator<?> i = ((OntModel) onto.getOntology()).listObjectsOfProperty(dp);
	
	while (i.hasNext()) {
	    Literal lit = (Literal) i.next();
	    String value = lit.getLexicalForm();
	    terms.add(new Annotation(value,lit.getLanguage()));
    	}	
	return terms;
    }

    protected Set<Annotation> getObjectPSpecificTerms(Object aClass, LoadedOntology<?> onto) {
	Set<Annotation> terms = new HashSet<Annotation>();
	ObjectProperty op = (ObjectProperty) aClass;
	// System.out.println(aClass);
	Iterator<?> i = ((OntModel) onto.getOntology()).listObjectsOfProperty(op).andThen(((OntModel) onto.getOntology()).listSubjectsWithProperty(op));
	while (i.hasNext()) {
	    Object current = i.next();
	    if (current instanceof Resource) {
		Resource res = (Resource) current;
		getIndividualTerms((Individual) res.as(Individual.class), terms);
	    } else if (current instanceof Literal) {
		Literal lit = (Literal) current;
		terms.add(new Annotation(lit.getLexicalForm(),lit.getLanguage()));
	    }

	}
	return terms;
    }

    @Override
    protected Set<URI> getSuperEntities(URI entity, HeavyLoadedOntology<?> onto) {
	OntResource res;
	Set<URI> uris = new HashSet<URI>();
	try {
	    res = (OntResource) onto.getEntity(entity);
	    Iterator<?> i;
	    if (res.isClass()) {
		i = res.asClass().listSuperClasses(true);
	    } else if (res.isDatatypeProperty()) {
		i = res.asDatatypeProperty().listSuperProperties(true);
	    } else if (res.isObjectProperty()) {
		i = res.asObjectProperty().listSuperProperties(true);
	    } else
		i = Collections.emptyList().iterator();

	    while (i.hasNext()) {
		String uri = ((OntResource) i.next()).getURI();
		if (uri != null)
		    uris.add(URI.create(uri));
	    }
	} catch (OntowrapException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return uris;
    }

}
