/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   Equivalence.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.alignment;

import org.semanticweb.owl.align.AlignmentException;

import fr.inrialpes.exmo.align.impl.rel.EquivRelation;
import fr.inrialpes.exmo.aroma.ontology.entities.AEntity;

public class Equivalence extends MatchingRelation {

    public Equivalence(AEntity ent1, AEntity ent2, double value)
	    throws AlignmentException {
	super(ent1, ent2, new EquivRelation(), value);
    }

    public boolean isGenerativeOf(MatchingRelation rel) {
	if (rel.equals(this)) return true;
	if (!(rel instanceof Implication)) return false;
	Implication imp = (Implication) rel;
	if ((object1.isAncestorOf(imp.getSource()) && this.object2.isDescendantOf(imp.getDest())) ||
		(object2.isAncestorOf(imp.getSource()) && this.object1.isDescendantOf(imp.getDest())) )
	    return true;
	return false;
    }
}
