/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   AROMA.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma;

import java.io.File;
import java.net.URI;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.semanticweb.owl.align.Alignment;
import org.semanticweb.owl.align.AlignmentException;
import org.semanticweb.owl.align.AlignmentProcess;
import org.semanticweb.owl.align.Cell;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.wcohen.ss.JaroWinkler;

import fr.inrialpes.exmo.align.impl.ObjectAlignment;
import fr.inrialpes.exmo.align.impl.rel.EquivRelation;
import fr.inrialpes.exmo.aroma.algorithms.ImplicationDiscovery;
import fr.inrialpes.exmo.aroma.algorithms.PostAlgorithms;
import fr.inrialpes.exmo.aroma.algorithms.PostAlignment;
import fr.inrialpes.exmo.aroma.alignment.AlignmentFactory;
import fr.inrialpes.exmo.aroma.alignment.Implication;
import fr.inrialpes.exmo.aroma.alignment.MatchingRelation;
import fr.inrialpes.exmo.aroma.measures.cardbased.Confidence;
import fr.inrialpes.exmo.aroma.measures.cardbased.Index;
import fr.inrialpes.exmo.aroma.measures.cardbased.IntensiteEtIPEE;
import fr.inrialpes.exmo.aroma.measures.cardbased.IntensiteImplication;
import fr.inrialpes.exmo.aroma.measures.set.WeightedMaxSum;
import fr.inrialpes.exmo.aroma.ontology.entities.AEntity;
import fr.inrialpes.exmo.aroma.ontology.entities.AbstractAEntityFactory;
import fr.inrialpes.exmo.aroma.ontology.entities.GenericAEntityFactory;
import fr.inrialpes.exmo.aroma.ontology.entities.JENAAEntityFactory;
import fr.inrialpes.exmo.ontosim.Measure;
import fr.inrialpes.exmo.ontosim.entity.EntityLexicalMeasure;
import fr.inrialpes.exmo.ontosim.entity.model.Entity;
import fr.inrialpes.exmo.ontosim.string.StringMeasureSS;
import fr.inrialpes.exmo.ontowrap.HeavyLoadedOntology;
import fr.inrialpes.exmo.ontowrap.LoadedOntology;
import fr.inrialpes.exmo.ontowrap.Ontology;
import fr.inrialpes.exmo.ontowrap.OntologyFactory;
import fr.inrialpes.exmo.ontowrap.OntowrapException;
import fr.inrialpes.exmo.ontowrap.jena25.JENAOntology;
import fr.inrialpes.exmo.ontowrap.jena25.JENAOntologyFactory;
import fr.inrialpes.exmo.ontowrap.skoslite.SKOSLiteOntologyFactory;
import fr.inrialpes.exmo.ontowrap.skoslite.SKOSLiteThesaurus;

public class AROMA extends ObjectAlignment implements AlignmentProcess {

    public AbstractAEntityFactory fact = new JENAAEntityFactory(false);

    // Default parameters
    protected double lexicalSimThreshold = 0.75;

    protected double ruleThreshold =Double.NaN;

    protected Index ruleIndex = new  IntensiteImplication();//IntensiteEtIPEE();//

    protected boolean removeCycles = true;

    protected boolean functionnal = true;

    protected boolean lexicalSim = true;
    
    public boolean skos=true;
    
    public int consensus = 1;

    public AROMA() {
	super();
    }

    public void init(Object onto1, Object onto2) throws AlignmentException {	
	OntologyFactory fact = new JENAOntologyFactory();
	
	try {
	    if (skos) {
		fact = new SKOSLiteOntologyFactory();
		this.fact= new GenericAEntityFactory(false);
		//onto1 = new SKOSLiteThesaurus((URI) onto1);
		//onto2 = new SKOSLiteThesaurus((URI) onto2);
		//System.err.println("SKOS");
	    }
	    
	    if (onto1 instanceof URI && onto2 instanceof URI) {
		onto1 = fact.loadOntology((URI) onto1);
        	onto2 = fact.loadOntology((URI) onto2);
	    } else if (onto1 instanceof SKOSLiteThesaurus && onto2 instanceof SKOSLiteThesaurus && !skos){
		this.fact= new GenericAEntityFactory(false);
	    }
	    else if ((onto1 instanceof Ontology<?> && onto2 instanceof Ontology<?>)
		    && !(onto1 instanceof JENAOntology && onto2 instanceof JENAOntology) && !skos) {
		onto1 = fact.loadOntology(((LoadedOntology<?>) onto1).getFile());
		onto2 = fact.loadOntology(((LoadedOntology<?>) onto2).getFile());
	    }
	    //System.err.println("loaded");
	}
	catch (OntowrapException e) {
	    throw new AlignmentException(e.getMessage());
	}

	super.init(onto1, onto2);

    }

    protected void parseParam(Properties param) {
	try {
	    lexicalSimThreshold = Double.parseDouble(param.getProperty("LexicalSimThreshold").toString());
	} catch (NumberFormatException e) {
	} catch (NullPointerException e) {
	}
	
	try {
	    if (param.containsKey("lexicalSim"))
		lexicalSim = "true".equals(param.getProperty("lexicalSim").toString());
	} catch (NullPointerException e) {
	}

	try {
	    ruleThreshold = Double.parseDouble(param.getProperty("RuleThreshold").toString());
	} catch (NumberFormatException e) {
	} catch (NullPointerException e) {
	}
	
	//Cannot do this : init is called before param
	/*try {
	    skos = "true".equals(param.getProperty("skos").toString());
	} catch (NullPointerException e) {
	}*/

	try {
	    String ruleIndexS = param.getProperty("RuleIndex").toString();
	    ruleIndex = (Index) Class.forName(ruleIndexS).newInstance();
	} catch (NullPointerException e) {
	} catch (InstantiationException e) {
	} catch (IllegalAccessException e) {
	} catch (ClassNotFoundException e) {
	} catch (ClassCastException e) {
	}
	
	

	// parseBoolean return false if the parameter is null or not equals to
	// "true"
	/*
	 * try { removeCycles =
	 * Boolean.parseBoolean(param.getParameter("RemoveCycles").toString());
	 * } catch (NullPointerException e) {} try { functionnal =
	 * Boolean.parseBoolean(param.getParameter("Functionnal").toString()); }
	 * catch (NullPointerException e) {} try { lexicalSim =
	 * Boolean.parseBoolean
	 * (param.getParameter("LecxicalSimMatcher").toString()); } catch
	 * (NullPointerException e) {}
	 */

	/*try {
	    String language = param.getProperty("stemmerLanguage").toString();
	} catch (NullPointerException e) {
	}*/
	
	try {
	    String fUris = param.getProperty("forbiddenUris").toString();
	    String[] uris = fUris.split(",");
	    Set<URI> forbiddenURIs = new HashSet<URI>();
	    for (String uri : uris) {
		try {
		    forbiddenURIs.add(URI.create(uri));
		}
		catch (IllegalArgumentException  e){}
		fact.setForbiddenURIs(forbiddenURIs);
	    }
	    
	}
	catch (NullPointerException e) {
	}
		
	try {
	    consensus = Integer.parseInt(param.getProperty("consensus"));
	}
	catch (NullPointerException e) {
	}
	catch (NumberFormatException e){
	}
	
	//TODO: Load instances from RDF file and assign them to ontology models
	try {
	    String instances1 = param.getProperty("instances1").toString();
	    
	    URI instances1Uri;
	    try {
		instances1Uri = URI.create(instances1);
	    }
	    catch (IllegalArgumentException e) {
		instances1Uri = (new File(instances1)).toURI();
	    }
	    //File instances1 = new File();
	    Model m1 = ModelFactory.createDefaultModel();
	    m1.read(instances1Uri.toString());
	    ((OntModel) this.ontology1().getOntology()).add(m1);
	    
	} catch (NullPointerException e) {
	}

    }

    public void align(Alignment a, Properties param) throws AlignmentException {
	
	if (param != null) {
	    parseParam(param);
	}
	loadInit(a);
	
	// Classes Alignment

	AEntity[] rootsC = loadCHierarchies();
	Set<MatchingRelation> alignC = new HashSet<MatchingRelation>();
	Thread tC = getAlignThread(rootsC[0], rootsC[1], alignC);
	tC.start();

	AEntity[] rootsP = loadPHierarchies();
	Set<MatchingRelation> alignP = new HashSet<MatchingRelation>();
	Thread tP = getAlignThread(rootsP[0], rootsP[1], alignP);
	tP.start();

	
	try {
	    tC.join();
	    tP.join();
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
	alignC.addAll(alignP);

	for (MatchingRelation mr : alignC) {
	    this.addCell(mr);
	}
    }

    private AEntity[] loadCHierarchies() {
	final AEntity[] roots = new AEntity[2];
	Thread t1 = new Thread() {
	    public void run() {
		roots[0] = fact.getClassHierarchy((HeavyLoadedOntology<?>)ontology1());
	    }
	};
	Thread t2 = new Thread() {
	    public void run() {
		roots[1] = fact.getClassHierarchy((HeavyLoadedOntology<?>)ontology2());
	    }
	};
	t1.start();
	t2.start();
	try {
	    t1.join();
	    t2.join();
	} catch (InterruptedException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	fact.free();
	return roots;
    }

    private AEntity[] loadPHierarchies() {
	final AEntity[] roots = new AEntity[2];
	Thread t1 = new Thread() {
	    public void run() {
		roots[0] = fact.getPropertyHierarchy((HeavyLoadedOntology<?>)ontology1());
	    }
	};
	Thread t2 = new Thread() {
	    public void run() {
		roots[1] = fact.getPropertyHierarchy((HeavyLoadedOntology<?>)ontology2());
	    }
	};
	t1.start();
	t2.start();
	try {
	    t1.join();
	    t2.join();
	} catch (InterruptedException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	fact.free();
	return roots;
    }

    private Thread getAlignThread(final AEntity root1, final AEntity root2, final Set<MatchingRelation> align) {
	return new Thread() {
	    public void run() {
		//System.err.println(root1.getSubEntities(false).size());
		AlignmentFactory af = new AlignmentFactory(root1, root2,ruleIndex);
		//PostAlignment.test(root1);
		// Eq alignment first decrease anatomy results
		PostAlignment.eqLabelAlign2(root1, root2, align, consensus, af);
		//PostAlignment.incLabelAlign(root1, root2, align, consensus, af);
                //PostAlignment.incLabelAlign(root2, root1, align, consensus, af);
		
		// need to be copy in case of automatic threshold computing
		double rt1=ruleThreshold;
		double rt2=ruleThreshold;
		ImplicationDiscovery idst = new ImplicationDiscovery(root1,root2, af, align, rt1);
		
		//System.err.println(idst.getThreshold());
		ImplicationDiscovery idts = new ImplicationDiscovery(root2,root1, af, align, rt2);
		//System.err.println(idts.getThreshold());

		Thread t1 = new Thread(idst);
		Thread t2 = new Thread(idts);

		t1.start();
		t2.start();

		try {
		    t1.join();
		    t2.join();
		} catch (InterruptedException e) {
		    e.printStackTrace();
		}

		Set<Implication> st = idst.getImplicationSet();
		Set<Implication> ts = idts.getImplicationSet();

		//System.err.println("END RULES");
		
		PostAlgorithms.deduceEquivalences(align, st, ts, af);
		
		PostAlgorithms.removeRedundancy(align);
		
		if (removeCycles) {
		    PostAlgorithms.removeCycles(align, af);
		}
		
		// true == functional alignment required
		if (functionnal) {
		    PostAlgorithms.retainMax(align, true, true);
		}
		
		Iterator<MatchingRelation> it = align.iterator(); 
		while (it.hasNext()) {
		    Cell c = it.next();
		    if (c instanceof Implication)
			//c.setRelation(new EquivRelation());
			it.remove();
		}
		
		//System.err.println("END CLEANING");
		

		// Syntactic matching : sometimes time-consuming...
		if (lexicalSim) {
		    PostAlignment.eqLabelAlign2(root1, root2, align, consensus, af);
		    //System.err.println("END EQ ALIGN");
		    Measure<Entity<?>> m = new EntityLexicalMeasure(new WeightedMaxSum<String>(new StringMeasureSS(new JaroWinkler())));
		    int oldSize = 0;
		    while (oldSize < align.size()) {
			oldSize = align.size();
			PostAlignment.alignSyntax(root1, root2, align, af, m, lexicalSimThreshold);
		    }
		    
		    //PostAlignment.alignParentWhenChildrenMatch(root1,root2,align,af);
		    if (removeCycles)
			PostAlgorithms.removeCycles(align, af);
			
			
		    //for (MatchingRelation mr : align) {
		    // mr.getObject1().freeTerms();
		   //	mr.getObject2().freeTerms();
		    //}
			
		    //System.err.println("END FUNCTIONAL");
		    PostAlgorithms.removeRedundancy(align);
		}
		

	    }
	};
    }
}
