/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   VerySimpleTE.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.utils.ling;

import java.util.HashSet;
import java.util.Set;

import org.apache.lucene.analysis.StopAnalyzer;
import org.apache.lucene.util.Version;

public class VerySimpleTE implements TermExtractor {

    StopAnalyzer stopAnalyser = new StopAnalyzer(Version.LUCENE_30);

    public String getTermType() {
	return "single";
    }

    public Set<String> getTerms(String text) {
	char[] tab = text.toLowerCase().toCharArray();
	Set<String> words = new HashSet<String>();
	StringBuilder current = null;
	int oldType = -1;
	for (char car : tab) {
	    int type = Character.getType(car);
	    if (type != oldType) {
		if (current != null)
		    words.add(current.toString());
		current = new StringBuilder();
	    }
	    if (Character.isLetterOrDigit(car))
		current.append(car);
	    oldType = type;
	}
	if (current != null)
	    words.add(current.toString());
	return words;
    }
    
    public Set<String> getTerms(String text, String language) {
	return getTerms(text);
    }

    /* (non-Javadoc)
     * @see fr.inrialpes.exmo.aroma.utils.ling.TermExtractor#free()
     */
    @Override
    public void free() {
	// TODO Auto-generated method stub
	
    }



}
