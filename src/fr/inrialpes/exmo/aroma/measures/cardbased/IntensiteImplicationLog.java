/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   IntensiteImplicationLog.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.measures.cardbased;

/**
 * @author jerome
 * 
 *         To change the template for this generated type comment go to
 *         Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class IntensiteImplicationLog extends Index {

    /*
     * (non-Javadoc)
     * 
     * @see modele.Indice#getIndice(int, int, int, int)
     */
    public double getIndice(int na, int nb, int nab, int n) {
	double lambda = ((double) Math.log(na) * Math.log(n - nb))
		/ ((double) Math.log(n));
	double sum = 1.0;
	long naNonb = Math.round(Math.log(na - nab));
	double exp_moinsLambda = Math.exp(-lambda);
	double val = 1.0;

	for (int k = 1; k <= naNonb; k++) {
	    val = val * (lambda / k);
	    sum += val;
	}
	if (naNonb == 0) {
	    sum = 0;
	} else {
	    sum = sum * exp_moinsLambda;
	}
	double ii = 1 - sum;
	return ii;
    }

}
