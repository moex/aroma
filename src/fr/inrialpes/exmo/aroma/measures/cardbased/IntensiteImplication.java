/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   IntensiteImplication.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.measures.cardbased;

import org.apache.commons.math.MathException;
import org.apache.commons.math.distribution.PoissonDistribution;
import org.apache.commons.math.distribution.PoissonDistributionImpl;

/**
 * @author jerome
 * 
 *         To change the template for this generated type comment go to
 *         Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class IntensiteImplication extends Index {

    /*
     * (non-Javadoc)
     * 
     * @see modele.Indice#getIndice(int, int, int, int)
     */
    public double getIndice(int na, int nb, int nab, int n) {
	double lambda = ((double) na * (n - nb)) / ((double) n);
	int naNonb = na - nab;
	if (lambda <= 0) {
	    return 0;
	}
	PoissonDistribution pdf = new PoissonDistributionImpl(lambda);
	try {
	    double value = 1.0 - pdf.cumulativeProbability(naNonb);
	    return value;
	} catch (MathException e) {
	    // System.err.println("balbla");
	    return Double.NaN;
	}
    }

}
