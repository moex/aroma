/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   Interestingness.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.measures.cardbased;

//

/**
 * @author Xuan-Hiep HUYNH
 * @version 1.0
 * 
 */
public class Interestingness {
    /** */
    private static double n;

    /** */
    private static double na;

    /** */
    private static double na_;

    /** */
    private static double nb;

    /** */
    private static double nb_;

    /** */
    private static double nab;

    /** */
    private static double nab_;

    /** */
    private static double na_b;

    /** */
    private static double na_b_;

    /** */

    /**
     *
     * 
     */
    public Interestingness() {
    }

    /**
     *
     * 
     */
    public static double getValue(final String name, final int nn,
	    final int nx, final int ny, final int nxy_) {
	n = (double) nn;
	na = (double) nx;
	nb = (double) ny;
	nab_ = (double) nxy_;
	//
	na_ = n - na;
	nb_ = n - nb;
	nab = na - nab_;
	na_b = nb - na + nab_;
	na_b_ = n - nb - nab_;
	//
	if (name.equals("Causal Confidence"))
	    return imCausalConfidence();
	else if (name.equals("Causal Confirm"))
	    return imCausalConfirm();
	else if (name.equals("Causal Confirmed-Confidence"))
	    return imCausalConfirmedConfidence();
	else if (name.equals("Causal Support"))
	    return imCausalSupport();
	else if (name.equals("Centered Confidence"))
	    return imCenteredConfidence();
	else if (name.equals("Certainty Factor"))
	    return imCertaintyFactor();
	else if (name.equals("CoefficientC"))
	    return imCoefficientC();
	else if (name.equals("Collective Strength"))
	    return imCollectiveStrength();
	else if (name.equals("Confidence"))
	    return imConfidence();
	else if (name.equals("Conviction"))
	    return imConviction();
	else if (name.equals("Cosine"))
	    return imCosine();
	else if (name.equals("Cramer"))
	    return imCramer();
	else if (name.equals("Dependency"))
	    return imDependency();
	else if (name.equals("Descriptive Confirm"))
	    return imDescriptiveConfirm();
	else if (name.equals("Descriptive Confirmed-Confidence"))
	    return imDescriptiveConfirmedConfidence();
	else if (name.equals("Dice"))
	    return imDice();
	else if (name.equals("EII"))
	    return imEII();
	else if (name.equals("EII 2"))
	    return imEII2();
	else if (name.equals("Example & Contra-Example"))
	    return imExampleContraExample();
	else if (name.equals("F-measure"))
	    return imFMeasure();
	else if (name.equals("Ganascia"))
	    return imGanascia();
	else if (name.equals("Gini-index"))
	    return imGiniIndex();
	else if (name.equals("II"))
	    return imII();
	else if (name.equals("Implication Index"))
	    return imImplicationIndex();
	else if (name.equals("Inclusion"))
	    return imInclusion();
	else if (name.equals("Information Gain"))
	    return imInformationGain();
	else if (name.equals("IPEE"))
	    return imIPEE();
	else if (name.equals("IQC"))
	    return imIQC();
	else if (name.equals("Jaccard"))
	    return imJaccard();
	else if (name.equals("J-measure"))
	    return imJMeasure();
	else if (name.equals("J-measure variant"))
	    return imJMeasureVariant();
	else if (name.equals("Kappa"))
	    return imKappa();
	else if (name.equals("Khi-2"))
	    return imKhi2();
	else if (name.equals("Klosgen"))
	    return imKlosgen();
	else if (name.equals("Kulczynski"))
	    return imKulczynski();
	else if (name.equals("Lambda-Coefficient"))
	    return imLambdaCoefficient();
	else if (name.equals("Laplace"))
	    return imLaplace();
	else if (name.equals("Least Contradiction"))
	    return imLeastContradiction();
	else if (name.equals("Lift"))
	    return imLift();
	else if (name.equals("Loevinger"))
	    return imLoevinger();
	else if (name.equals("MaxConf"))
	    return imMaxConf();
	else if (name.equals("Mutual Information"))
	    return imMutualInformation();
	else if (name.equals("Ochiai"))
	    return imOchiai();
	else if (name.equals("Odd Multiplier"))
	    return imOddMultiplier();
	else if (name.equals("Odds Ratio"))
	    return imOddsRatio();
	else if (name.equals("Pavillon"))
	    return imPavillon();
	else if (name.equals("Pearl"))
	    return imPearl();
	else if (name.equals("Phi-Coefficient"))
	    return imPhiCoefficient();
	else if (name.equals("Putative Causal Dependency"))
	    return imPutativeCausalDependency();
	else if (name.equals("Rand-index"))
	    return imRandIndex();
	else if (name.equals("Rogers & Tanimoto"))
	    return imRogersTanimoto();
	else if (name.equals("Rule Interest"))
	    return imRuleInterest();
	else if (name.equals("Rule Interest variant"))
	    return imRuleInterestVariant();
	else if (name.equals("Russel & Rao"))
	    return imRusselRao();
	else if (name.equals("Sebag & Schoenauer"))
	    return imSebagSchoenauer();
	else if (name.equals("Similitude"))
	    return imSimilitude();
	else if (name.equals("Similarity Index"))
	    return imSimilarityIndex();
	else if (name.equals("Simple Match"))
	    return imSimpleMatch();
	else if (name.equals("Support"))
	    return imSupport();
	else if (name.equals("Suprise"))
	    return imSuprise();
	else if (name.equals("Taux de liaison"))
	    return imTauxDeLiaison();
	else if (name.equals("TIC"))
	    return imTIC();
	else if (name.equals("Yule's Q"))
	    return imYuleQ();
	else if (name.equals("Yule's Y"))
	    return imYuleY();
	else if (name.equals("Zhang"))
	    return imZhang();
	//
	else {
	    System.err.println("ERROR. Unknown measure: " + name);
	}
	//
	return 0; // For compilation only, not used.
    }

    /**
     *
     * 
     */
    private static double imCausalConfidence() {
	if (nb_ == 0)
	    return Double.NaN;
	return (double) 1.0 - 0.5 * ((1.0 / na) + (1.0 / nb_)) * nab_;
    }

    /**
     *
     * 
     */
    private static double imCausalConfirm() {
	return (double) (na + nb_ - 4.0 * nab_) / n;
    }

    /**
     *
     * 
     */
    private static double imCausalConfirmedConfidence() {
	if (nb_ == 0)
	    return Double.NaN;
	return (double) 1.0 - 0.5 * ((3.0 / na) + (1.0 / nb_)) * nab_;
    }

    /**
     *
     * 
     */
    private static double imCausalSupport() {
	return (double) (na + nb_ - 2.0 * nab_) / n;
    }

    /**
     *
     * 
     */
    private static double imCenteredConfidence() {
	return (double) (n * nab - na * nb) / (n * na);
    }

    /**
     * 
     * Tan 2000
     */
    private static double imCertaintyFactor() {
	return (double) Math.max(1 - (n * nab_) / (na * nb_), (na * nb_ - n
		* nab_)
		/ (na_ * nb));
    }

    /**
     *
     * 
     */
    private static double imCoefficientC() {
	double k22 = imKhi2() * imKhi2();

	return (double) Math.sqrt(k22 / (k22 + n));
    }

    /**
     *
     * 
     */
    private static double imCollectiveStrength() {
	if ((nb - na + 2.0 * nab_) == 0)
	    return Double.NaN;
	return (double) ((na - nab_) * (nb_ - nab_) * (na * nb_ + nb * na_))
		/ ((na * nb + na_ * nb_) * (nb - na + 2.0 * nab_));
    }

    /**
     *
     * 
     */
    private static double imConfidence() {
	return (double) 1.0 - (nab_ / na);
    }

    /**
     *
     * 
     */
    private static double imConviction() {
	if (nab_ == 0)
	    return Double.NaN;
	return (double) (na * nb_) / (n * nab_);
    }

    /**
     *
     * 
     */
    private static double imCosine() {
	return (double) (na - nab_) / Math.sqrt(na * nb);
    }

    /** */
    private static double imCramer() {
	return (double) Math.sqrt(imKhi2() / n);
    }

    /**
     *
     * 
     */
    private static double imDependency() {
	return (double) Math.abs(nb_ / n - nab_ / na);
    }

    /**
     *
     * 
     */
    private static double imDescriptiveConfirm() {
	return (double) (na - 2.0 * nab_) / n;
    }

    /**
     *
     * 
     */
    private static double imDescriptiveConfirmedConfidence() {
	return (double) 1.0 - 2.0 * (nab_ / na);
    }

    /**
     *
     *
     */
    private static double imDice() {
	return (double) (2 * nab) / (2 * nab + nab_ + na_b);
    }

    /**
     * 
     * entropic imlication intensity (alpha = 1)
     */
    private static double imEII() {
	double alpha = 1;
	//
	if ((nb_ == 0) || (n == nb))
	    return Double.NaN;
	return FastII.entropic(n, na, nb, nab_, alpha);
    }

    /**
     * 
     * entropic implication intensity (alpha = 2)
     */
    private static double imEII2() {
	double alpha = 2;
	//
	if ((nb_ == 0) || (n == nb))
	    return Double.NaN;
	return FastII.entropic(n, na, nb, nab_, alpha);
    }

    /**
     *
     *
     */
    private static double imExampleContraExample() {
	if (na - nab_ == 0)
	    return Double.NaN;
	return (double) 1.0 - nab_ / (na - nab_);
    }

    /**
     *
     * 
     */
    private static double imFMeasure() {
	double recall = nab / na;
	double precision = nab / nb;
	return (double) (2 * recall * precision) / (precision + recall);
    }

    /**
     *
     * 
     */
    private static double imGanascia() {
	return (double) 1.0 - 2.0 * nab_ / na;
    }

    /**
     *
     * 
     */
    private static double imGiniIndex() {
	if (na_ == 0)
	    return Double.NaN;
	//
	double value = (na / n)
		* (Math.pow((na - nab_) / na, 2.0) + Math.pow(nab_ / na, 2.0));
	value = value
		+ na_
		/ n
		* (Math.pow((nb - na + nab_) / na_, 2.0) + Math.pow(
			na_b_ / na_, 2.0));
	value = value - (Math.pow(nb / n, 2.0) + Math.pow(nb_ / n, 2.0));
	return value;
    }

    /**
     * 
     * hyper-geometric implication intensity
     */
    private static double imII() {
	return FastII.hypergeometric(n, na, nb, nab_);
    }

    /**
     * 
     * implication index
     */
    private static double imImplicationIndex() {
	return (double) (nab_ - na * nb_ / n) / Math.sqrt(na * nb_ / n);
    }

    /**
     * 
     * 
     */
    private static double imInclusion() {
	double alpha = 2;
	//
	return (double) FastII.inclusion(n, na, nb, nab_, alpha);
    }

    /**
     * 
     * Church & Hanks 1990
     */
    private static double imInformationGain() {
	return (double) Math.log(imLift());
    }

    /**
     *
     * 
     */
    private static double imIPEE() {
	return (double) FastII.ipee(n, na, nb, nab_);
    }

    /**
     *
     * 
     */
    private static double imIQC() {
	return (double) 2 * (n * na - n * nab_ - na * nb)
		/ (n * na + n * nb - 2 * na * nb);
    }

    /**
     *
     * 
     */
    private static double imJaccard() {
	return (double) (na - nab_) / (nb + nab_);
    }

    /**
     *
     * 
     */
    private static double imJMeasure() {
	if ((nb_ == 0) || (nab_ == 0) || (na - nab_ == 0))
	    return Double.NaN;
	return (double) (na - nab_) / n * Math.log(n * (na - nab_) / (na * nb))
		+ nab_ / n * Math.log((n * nab_) / (na * nb_));
    }

    /**
     *
     * 
     */
    private static double imJMeasureVariant() {
	if (na - nab_ == 0)
	    return Double.NaN;
	return (double) (na - nab_) / n * Math.log(n * (na - nab_) / (na * nb));
    }

    /**
     *
     * 
     */
    private static double imKappa() {
	if ((na_ == 0) && (nb_ == 0))
	    return Double.NaN;
	return (double) (2.0 * (na * nb_ - n * nab_)) / (na * nb_ + na_ * nb);
    }

    /**
     *
     * 
     */
    private static double imKhi2() {
	// double a = (double) nab/n;
	// double b = (double) nab_/n;
	// double c = (double) na_b/n;
	// double d = (double) na_b_/n;
	//
	// return (double) n*Math.pow(a*d - b*c, 2)/((a + b)*(c + d)*(a + c)*(b
	// + d));
	return (double) (n * Math.pow(n * nab - na * nb, 2))
		/ (na * nb * na_ * nb_);
    }

    /*
     *
     * 
     */
    private static double imKlosgen() {
	return (double) Math.sqrt((na - nab_) / n) * (nb_ / n - nab_ / na);
    }

    /*
     * 
     * Lallich 2004
     */
    private static double imKulczynski() {
	return (double) (na - nab_) / (nb - na + 2 * nab_);
    }

    /**
     * Goodman-Kruskal's [Tan 2000]
     */
    private static double imLambdaCoefficient() {
	double max1 = Math.max(nab + na_b, nab_ + na_b_);
	double max2 = Math.max(nab + nab_, na_b + na_b_);
	//
	return (double) (Math.max(nab, nab_) + Math.max(na_b, na_b_)
		+ Math.max(nab, na_b) + Math.max(nab_, na_b_) - max1 - max2)
		/ (2 * n - max1 - max2);
    }

    /**
     *
     * 
     */
    private static double imLaplace() {
	return (double) (1.0 + na - nab_) / (na + 2.0);
    }

    /**
     *
     * 
     */
    private static double imLeastContradiction() {
	return (double) (na - 2.0 * nab_) / nb;
    }

    /**
     * or Interest Factor [Brin 1997]
     */
    private static double imLift() {
	return (double) n * (na - nab_) / (na * nb);
    }

    /**
     * 
     * [Guillet 2004]
     */
    private static double imLoevinger() {
	if (nb_ == 0)
	    return Double.NaN;
	return (double) 1.0 - (n * nab_) / (na * nb_);
    }

    /**
     * 
     * [Tan 2000]
     */
    private static double imMaxConf() {
	return (double) Math.max(nab / na, nab / nb);
    }

    /**
     * 
     * [Tan 2000]
     */
    private static double imMutualInformation() {
	return (double) (nab / n * Math.log(n * nab / (na * nb)) + nab_ / n
		* Math.log(n * nab_ / (na * nb_)) + na_b / n
		* Math.log(n * na_b / (na_ * nb)) + na_b_ / n
		* Math.log(n * na_b_ / (na_ * nb_)))
		/ Math.min(-(na / n * Math.log(na / n) + na_ / n
			* Math.log(na_ / n)), -(nb / n * Math.log(nb / n) + nb_
			/ n * Math.log(nb_ / n)));
    }

    /**
     * 
     * [Saporta 1990]
     */
    private static double imOchiai() {
	return (double) (nab) / (Math.sqrt((nab + nab_) * (nab + na_b)));
    }

    /**
     * 
     * [Lallich and Teytaud 2004]
     */
    private static double imOddMultiplier() {
	return (double) ((na - nab_) * nb_) / (nb * nab_);
    }

    /**
     * 
     * Tan 2002
     */
    private static double imOddsRatio() {
	if ((nab_ == 0) || ((nb - na + nab_) == 0))
	    return Double.NaN;
	return (double) ((na - nab_) * (nb_ - nab_))
		/ (nab_ * (nb - na + nab_));
    }

    /**
     *
     * 
     */
    private static double imPavillon() {
	return (double) nb_ / n - nab_ / na;
    }

    /**
     *
     * 
     */
    private static double imPearl() {
	return (double) Math.abs((n * nab_ - na * nb_) / n);
    }

    /**
     *
     * 
     */
    private static double imPhiCoefficient() {
	if ((na_ == 0) || (nb_ == 0))
	    return Double.NaN;
	return (double) (na * nb_ - n * nab_) / Math.sqrt(na * nb * na_ * nb_);
    }

    /**
     *
     * 
     */
    private static double imPutativeCausalDependency() {
	if (nb_ == 0)
	    return Double.NaN;
	return (double) 1.5 + (4.0 * na - 3.0 * nb) / (2.0 * n)
		- (3.0 / (2.0 * na) + 2.0 / nb_) * nab_;
    }

    /**
     *
     * 
     */
    private static double imRandIndex() {
	return (double) (nab + na_b_) / (nab + nab_ + na_b + na_b_);
    }

    /**
     *
     * 
     */
    private static double imRogersTanimoto() {
	return (double) (nab + na_b_) / (nab + na_b_ + 2 * (nab_ + na_b));
    }

    /**
     *
     * 
     */
    private static double imRuleInterest() {
	if (n - nab == 0)
	    return Double.NaN;
	return (double) (1.0 / n) * ((na * nb_) / n - nab_);
    }

    /**
     *
     * 
     */
    private static double imRuleInterestVariant() {
	if (n - nab == 0)
	    return Double.NaN;
	return (double) (1.0 / n) * Math.abs((na * nb_) / n - nab_);
    }

    /**
     *
     * 
     */
    private static double imRusselRao() {
	return (double) (nab) / (nab + nab_ + na_b + na_b_);
    }

    /**
     *
     * 
     */
    private static double imSebagSchoenauer() {
	if (nab_ == 0)
	    return Double.NaN;
	return (double) na / nab_ - 1.0;
    }

    /**
     *
     * 
     */
    private static double imSimilitude() {
	return (double) n * (na - nab_) / (na + nb);
    }

    /**
     *
     * 
     */
    private static double imSimilarityIndex() {
	return (double) (na - nab_ - na * nb / n) / Math.sqrt(na * nb / n);
    }

    /**
     *
     * 
     */
    private static double imSimpleMatch() {
	return (double) (nab + na_b_) / n;
    }

    /**
     *
     * 
     */
    private static double imSupport() {
	return (double) (na - nab_) / n;
    }

    /**
     *
     * 
     */
    private static double imSuprise() {
	return (double) (nab - nab_) / nb;
    }

    /**
     *
     * 
     */
    private static double imTauxDeLiaison() {
	return (double) imLift() - 1.0;
    }

    /**
     *
     * 
     */
    private static double imTIC() {
	return FastII.tic(n, na, nb, nab_);
    }

    /**
     *
     * 
     */
    private static double imYuleQ() {
	// if ((nab_ == 0) || ((nb - na + nab_) == 0)) return Double.NaN;
	if ((nab_ == 0) || ((nb - na + nab_) == 0))
	    return 1.0;
	//
	double alpha = (na - nab_) / nab_ * (n - nb - nab_) / (nb - na + nab_);
	return (double) (alpha - 1.0) / (alpha + 1.0);
    }

    /**
     *
     * 
     */
    private static double imYuleY() {
	// if ((nab_ == 0) || ((nb - na + nab_) == 0)) return Double.NaN;
	if ((nab_ == 0) || ((nb - na + nab_) == 0))
	    return 1.0;
	//
	double alpha = (na - nab_) / nab_ * (n - nb - nab_) / (nb - na + nab_);
	return (double) (Math.sqrt(alpha) - 1.0) / (Math.sqrt(alpha) + 1.0);
    }

    /**
     *
     * 
     */
    private static double imZhang() {
	return (double) (n * nab - na * nb) / Math.max(nab * nb_, nb * nab_);
    }

}
