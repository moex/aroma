/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   AROMABatchMode.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma;

import java.io.File;
import java.io.PrintWriter;

import fr.inrialpes.exmo.align.impl.BasicAlignment;
import fr.inrialpes.exmo.align.impl.renderer.RDFRendererVisitor;

public class AROMABatchMode {

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
	if (args.length < 1) {
	    System.err.println("USAGE : java AROMABatchMode ontoDir \n"
			    + " aligns all ontologies contained in ontoDir and output alignments in alignDir");
	    System.exit(-1);
	}

	PrintWriter pw = new PrintWriter(System.out);
	RDFRendererVisitor rrv = new RDFRendererVisitor(pw);

	File ontoDir = new File(args[0]);
	File[] ontoFiles = ontoDir.listFiles();

	for (int i = 0; i < ontoFiles.length; i++) {
	    try {
		for (int j = i + 1; j < ontoFiles.length; j++) {
		    AROMA align = new AROMA();
		    align.init(ontoFiles[i].toURI(), ontoFiles[j].toURI());
		    align.align(new BasicAlignment(), null);
		    rrv.visit(align);
		}
	    } catch (Exception e) {
		e.printStackTrace();
	    }

	}
	pw.close();

    }

}
