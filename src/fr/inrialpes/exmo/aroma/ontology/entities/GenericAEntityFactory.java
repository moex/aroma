/**
 *   This file is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.ontology.entities;

import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.hp.hpl.jena.vocabulary.OWL;

import fr.inrialpes.exmo.ontowrap.Annotation;
import fr.inrialpes.exmo.ontowrap.HeavyLoadedOntology;
import fr.inrialpes.exmo.ontowrap.LoadedOntology;
import fr.inrialpes.exmo.ontowrap.OntologyFactory;
import fr.inrialpes.exmo.ontowrap.OntowrapException;

/**
 * Basic and ontowrap generic implementation of AEntityFactory
 * This impl does not retreive property specific terms
 * @author jerome David
 *
 */
public class GenericAEntityFactory extends AbstractAEntityFactory {

    public GenericAEntityFactory(boolean useCache) {
	super(useCache);
    }

    public GenericAEntityFactory(boolean useCache, Set<URI> forbiddenURIs) {
	super(useCache, forbiddenURIs);
    }

    protected AEntityImpl createARoot(LoadedOntology<?> onto) {
	return new AEntityImpl(onto, OWL.Thing);
    }
    
    private void getIndividualTerms(Object ind, Set<Annotation> terms, LoadedOntology<?> onto) {
	URI entURI=null;
	try {
	    entURI = onto.getEntityURI(ind);
	} catch (OntowrapException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	if (entURI != null) {
	    terms.add(new Annotation(entURI.getFragment()));
	    try {
		terms.addAll(onto.getEntityAnnotationsL(ind));
	    } catch (OntowrapException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }
	}
    }

    protected Set<Annotation> getClassSpecificTerms(Object aClass, HeavyLoadedOntology<?> onto) {
	Set<Annotation> terms = new HashSet<Annotation>();
	Set<?> s;
	try {
	    s = onto.getInstances(aClass, OntologyFactory.DIRECT, OntologyFactory.ASSERTED, OntologyFactory.NAMED);
	    for (Object o : s) {
		    getIndividualTerms(o, terms,onto);
	    }
	} catch (OntowrapException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
	return terms;
    }

    protected Set<Annotation> getDataPSpecificTerms(Object aClass, LoadedOntology<?> onto) {
	return Collections.emptySet();
    }



    protected Set<Annotation> getObjectPSpecificTerms(Object aClass, LoadedOntology<?> onto) {
	return Collections.emptySet();
    }

    protected Set<URI> getSuperEntities(URI entityUri, HeavyLoadedOntology<?> onto) {
	try {
	    Object entity = onto.getEntity(entityUri);
	    Set<? extends Object> superEnt=Collections.emptySet();
	    if (onto.isClass(entity)) {
		superEnt = onto.getSuperClasses(entity,OntologyFactory.DIRECT, OntologyFactory.ASSERTED, OntologyFactory.NAMED);
	    }
	    else if (onto.isProperty(entity)) {
		superEnt = onto.getSuperProperties(entity,OntologyFactory.DIRECT, OntologyFactory.ASSERTED, OntologyFactory.NAMED);
	    }
	    HashSet<URI> uris = new HashSet<URI>();
	    for (Object o : superEnt)
		try {
		    uris.add(onto.getEntityURI(o));
		}
	    	catch (OntowrapException e) {}
	    return uris;
	} catch (OntowrapException e) {
	    e.printStackTrace();
	}
	return null;
	
    }

}
