/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   TI.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.measures.cardbased;

public class TI extends Index {

    public TI() {
	super();
    }

    public double getIndice(int na, int nb, int nab, int n) {
	double Hreduite_b = Hreduite(nb, n);
	return (Hreduite_b - Hreduite(nab, na)) / (Hreduite_b);
    }

    protected double H(int na, int n) {
	double r1 = ((double) na) / n;
	double r2 = ((double) (n - na)) / n;

	if (r1 == 0 || r2 == 0) {
	    return Double.NEGATIVE_INFINITY;
	}
	
	double result = -r1 * log2(r1) - r2 * log2(r2);
	
	return result;

    }

    protected double Hreduite(int na, int n) {
	if (na >= (((double) n) / 2)) {
	    return H(na, n);
	}
	return 1.0;
    }

}
