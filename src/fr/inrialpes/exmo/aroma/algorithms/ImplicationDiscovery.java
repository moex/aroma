/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   ImplicationDiscovery.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

package fr.inrialpes.exmo.aroma.algorithms;

import java.io.PrintStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import fr.inrialpes.exmo.aroma.alignment.AlignmentFactory;
import fr.inrialpes.exmo.aroma.alignment.Implication;
import fr.inrialpes.exmo.aroma.alignment.MatchingRelation;
import fr.inrialpes.exmo.aroma.measures.cardbased.Index;
import fr.inrialpes.exmo.aroma.ontology.entities.AEntity;

/**
 * @author jerome DAVID The implication discovery algorithms of AROMA AROMA
 *         algorithms by Jerome DAVID new version, rewritten in 06/2008
 * 
 * 
 */
public class ImplicationDiscovery implements Runnable {
    private double threshold = 0.95;

    private Index measure;

    private AlignmentFactory af;

    private AEntity source;
 
    private AEntity dest;

    private final Set<Implication> implications;
    
    private final Set<MatchingRelation> correspondences;

    private int n;

    public ImplicationDiscovery(AEntity a1, AEntity a2, AlignmentFactory af, Set<MatchingRelation> correspondences) {
	this.af = af;
	measure = af.getIndex();
	source = a1;
	dest = a2;
	implications = new HashSet<Implication>();
	this.correspondences=correspondences;

	// int nh1 = source.getNbTerms();
	// int nh2 = dest.getNbTerms();

	source.retainTerms(dest);
	dest.retainTerms(source);
	n = dest.getNbTerms();

	this.threshold = measure.getIndice(n / 8, n / 4, n / 25, n);
	// for (String t : source.getTerms()) System.out.println(t);
	// double essT = measure.getIndice(nh1,nh2,n,nh1+nh2-n);
	// System.out.println("Old Threshold : "+threshold);
	// System.out.println("new threshold : "+essT);

    }

    public ImplicationDiscovery(AEntity a1, AEntity a2, AlignmentFactory af, Set<MatchingRelation> correspondences, double threshold) {
	this(a1, a2, af, correspondences);
	if (threshold >= 0 && threshold <= 1)
	    this.threshold = threshold;
    }

    /**
     * Recherche parmi dst est ses descendant les implications valides pour une
     * premisse "src" donnée les implications valides sont Ajoutées dans le Set
     * "implications"
     * 
     * @param src
     * @param dst
     * @param concEnCours
     *            N'est plus utilisé pour l'instant, mais à ne pas enlever car
     *            pourrait servir par la suite
     * @param maxVal
     *            la meilleure valeur trouvée.
     * @return
     */
    private double specialiseConc(AEntity src, AEntity dst) {
	// if dst has no term, then 0 is returned (stop the search)
	if (dst.getNbTerms() == 0) {
	    return 0;
	}

	/*if (src.getURI().toString().endsWith("115020") && dst.getURI().toString().endsWith("3401")) {
	    System.err.println("src : "+src.getNbTerms()+", dsrt : "+ dst.getNbTerms()+", common = "+src.getNbCommonTerms(dst));
	}*/
	
	// compute the quality value of src -> dst
	double value = measure.getIndice(src.getNbTerms(), dst.getNbTerms(),src.getNbCommonTerms(dst), n);
	double returnVal = value;

	if (Double.isNaN(value)) {
	    value = 0;
	}

	// if conf(na,nab,nab,n) > THRESHOLD, then the bottom search continues
	if ((measure.getIndice(src.getNbTerms(), src.getNbCommonTerms(dst), src.getNbCommonTerms(dst), n) >= threshold)) {
	    boolean bestChild = false;
	    for (AEntity child : dst.getSubEntities(true)) {
		double childVal = specialiseConc(src, child);
		if (childVal >= value) {
		    bestChild = true;
		    if (returnVal < childVal) {
			returnVal = childVal;
		    }
		}
	    }

	    if (!bestChild && (value >= threshold)) { // && (value >= maxII)) {
		Implication candidateImp = af.getImplication(src, dst);// new

		if (!(candidateImp.hasBestAndGenerativeImplication(implications)||
			candidateImp.hasBestAndGenerativeImplication(correspondences))) {
		    implications.add(candidateImp);
		}
	    }
	}
	return returnVal;
    }

    private void specialisePremisse(AEntity src, Set<AEntity> concEnCours) {

	Iterator<AEntity> concsIt = concEnCours.iterator();
	while (concsIt.hasNext()) {
	    AEntity conc = concsIt.next();
	    // if no common terms then the candidate conclusion is removed and
	    // no bottom search is done
	    if (src.getNbCommonTerms(conc) == 0)
		concsIt.remove();
	    else
		specialiseConc(src, conc);
	}

	for (AEntity child : src.getSubEntities(true)) {
	    specialisePremisse(child, new HashSet<AEntity>(concEnCours));
	}
    }

    public void printImpl(PrintStream out) {
	Iterator<Implication> i = implications.iterator();
	while (i.hasNext()) {
	    Implication imp = (Implication) i.next();
	    out.println(imp.getSource() + " -> " + imp.getDest());
	}
    }

    public Set<Implication> getImplicationSet() {
	return implications;
    }

    public void setThreshold(double thres) {
	threshold = thres;
    }

    public void run() {

	// recherche des implications src -> dst
	Collection<AEntity> concs = dest.getSubEntities(true);
	while (concs != null && concs.size() == 1) {
	    concs = ((AEntity) concs.iterator().next()).getSubEntities(true);
	}
	Collection<AEntity> premisses = source.getSubEntities(true);
	while (premisses != null && premisses.size() == 1) {
	    premisses = ((AEntity) premisses.iterator().next())
		    .getSubEntities(true);
	}

	if (concs != null && premisses != null && concs.size() > 0
		&& premisses.size() > 0) {
	    for (AEntity p : premisses) {
		specialisePremisse(p, new HashSet<AEntity>(concs));

	    }
	}
    }

    public double getThreshold() {
	return threshold;
    }

}
