/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   FastII.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

package fr.inrialpes.exmo.aroma.measures.cardbased;

//
/**
 * 
 * @author Xuan-Hiep HUYNH Convert to Java from the source code of Prof. Fabrice
 *         Guillet
 */
public class FastII extends Index {
    /** */
    private static double DBL_MIN = java.lang.Double.MIN_VALUE;

    /** */
    private static double DBL_MAX = java.lang.Double.MAX_VALUE;

    /** */
    private static double Cmin = DBL_MIN * 1.0e10;

    /** */
    private static double Cmax = DBL_MAX * 1.0e-10;

    /** */
    private static double DBL_EPSILON = Double.longBitsToDouble(1);

    /** Creates a new instance of ii */
    public FastII() {
    }

    @SuppressWarnings("unused")
    private static boolean conditionValidite(final int n, final int na,
	    final int nb, final int nanbNon) {
	return (n >= 0) && (na >= 0) && (nb >= 0) && (nanbNon >= 0)
		&& (na <= n) && (nb <= n) && (n - na <= n) && (n - nb <= n)
		&& (nanbNon <= na) && (nanbNon <= n - nb)
		&& (nanbNon >= Math.max(0, (na - nb)))
		&& (nanbNon <= Math.min(na, n - nb));
    }

    /**
     * 
     * Rapport de valeurs hypergeometriques
     */
    private static double ratioHp(final int n, final int na, final int nb,
	    final int k) {
	double r;
	//
	// assert( k>0 && k>max(0,int(na-nb)) );
	r = ((double) (na - k + 1) / (nb - na + k))
		* ((double) (n - nb - k + 1) / k);
	if ((r <= DBL_EPSILON) || (1 / r <= DBL_EPSILON)) {
	    // cerr << "**** line: " << __LINE__ << " file: " << __FILE__ ;
	    // cerr << "** errreur : r=" << r << "<=" << DBL_EPSILON <<
	    // " precision depassee\n";
	    System.err.println("** errreur : r=" + r + "<=" + DBL_EPSILON
		    + " precision depassee.");
	}
	//
	return r;
    }

    /**
     *
     *
     */
    private static double hpSuivant(final int n, final int na, final int nb,
	    final int k, final double hp_kmoins1) {
	double hp;
	//
	hp = hp_kmoins1 * ratioHp(n, na, nb, k);
	if (hp <= DBL_MIN) {
	    System.err.println("** errreur : hp=" + hp + "<=" + DBL_MIN
		    + " precision depassee.");
	}
	return hp;
    }

    /**
     *
     *
     */
    private static double hypergeometrique(final int n, final int na,
	    final int nb, final int k) {
	// cas 0 : na <= n-nb && na <= nb
	// Complexite ~= 2*na

	// conditions de validite
	// assert ( conditionValidite(n, na, nb, k) );
	// conditions d'efficacite :
	// assert( na <= n-nb && na <= nb );

	@SuppressWarnings("unused")
	int naNon, nbNon, nMoinsK, naMoinsK, maxi3;
	int i1, i2, i3;
	double valeurHp, u;
	boolean ok;
	//
	naNon = n - na;
	nbNon = n - nb;
	valeurHp = 1.0;
	//
	// calcul du cas particulier k = 0
	if (k == 0) {
	    if (nb != n) {
		for (int p = 0; p < na; p++) {
		    valeurHp *= (double) (nb - p) / (n - p);
		    if (valeurHp <= DBL_MIN)
			break;
		    // assert(valeurHp >= DBL_MIN && valeurHp <= DBL_MAX );
		}
	    }// fin du calcul du cas k=0
	    return valeurHp;
	}// fin du cas k=0

	// calcul du cas k > 0
	// assert( k>0 );
	nMoinsK = n - k;
	naMoinsK = na - k;
	maxi3 = Math.min(naMoinsK, k);
	i1 = 0;
	i2 = 0;
	i3 = 0;
	if (nbNon == n)
	    i1 = k;
	if (nb == nMoinsK)
	    i2 = naMoinsK;
	if (na == maxi3)
	    i3 = maxi3;
	while (i1 < k || i2 < naMoinsK || i3 < maxi3) {
	    ok = false;
	    while (i1 < k && valeurHp >= Cmin) {
		ok = true;
		u = (double) (nbNon - i1) / (n - i1);
		valeurHp *= u;
		i1++;
	    }
	    // assert(valeurHp >= DBL_MIN && valeurHp <= DBL_MAX );
	    while (i2 < naMoinsK && valeurHp >= Cmin) {
		ok = true;
		u = (double) (nb - i2) / (nMoinsK - i2);
		valeurHp *= u;
		i2++;
	    }
	    // assert(valeurHp >= DBL_MIN && valeurHp <= DBL_MAX );
	    while (i3 < maxi3 && valeurHp <= Cmax) {
		ok = true;
		u = (double) (na - i3) / (maxi3 - i3);
		valeurHp *= u;
		i3++;
	    }
	    // assert(valeurHp >= DBL_MIN && valeurHp <= DBL_MAX );
	    if (!ok) {
		// assert( valeurHp <= Cmin );
		return DBL_MIN;
	    }
	}
	// fin du cas k>0

	return valeurHp;
    }

    /**
     *
     * 
     */
    private static double sommeHp(final int n, final int na, final int nb,
	    final double epsilon, final int kmin, final int kmax,
	    final double hp_kmin, Pointer hp_kmax) {
	// cas 0 : na <= n-nb && na <= nb
	// Complexite ~= ?

	// conditions de validite
	// assert ( conditionValidite(n, na, nb, kmax) );
	// assert ( conditionValidite(n, na, nb, kmin) );
	// conditions d'efficacite :
	// assert( na <= n-nb && na <= nb );
	// assert( kmin<=kmax );

	int i;
	double phi, r, hp, sommer, erreurMax;

	if (kmax == kmin) {
	    Pointer.setValue(hp_kmin);
	    return hp_kmin;
	}

	hp = hp_kmin;
	phi = hp_kmin;
	for (i = kmin + 1; i <= kmax; i++) {
	    r = ratioHp(n, na, nb, i);
	    hp = hp * r;
	    phi = phi + hp;
	    if (hp <= epsilon) {
		if (kmax - i > 1) {
		    sommer = (1.0 / (1.0 - r));
		    if (sommer > kmax - i) {
			sommer = (double) kmax - i;
		    }
		    erreurMax = hp * sommer;
		    if (erreurMax <= epsilon) {
			break;
		    }
		} else {
		    // assert( i==kmax-1 || i==kmax);
		    break;
		}
	    }
	}
	Pointer.setValue(hp);
	//
	return phi;
    }

    private static double sommeHpInv(final int n, final int na, final int nb,
	    final double epsilon, final int kmin, final int kmax,
	    final double hp_kmax) {
	// cas 0 : na <= n-nb && na <= nb
	// Complexite ~= ?

	// conditions de validite
	// assert ( conditionValidite(n, na, nb, kmax) );
	// assert ( conditionValidite(n, na, nb, kmin) );
	// conditions d'efficacite :
	// assert( na <= n-nb && na <= nb );
	// assert( kmin<=kmax );

	int i;
	double phi, r, hp, sommer, erreurMax;

	if (kmax == kmin) {
	    return hp_kmax;
	}

	hp = hp_kmax;
	phi = hp_kmax;
	for (i = kmax; i > kmin; i--) {
	    r = 1.0 / ratioHp(n, na, nb, i);
	    hp = hp * r;
	    phi = phi + hp;
	    if (hp <= epsilon) {
		if (i - kmin > 1) {
		    sommer = (1.0 / (1.0 - r));
		    if (sommer > i - kmin) {
			sommer = (double) i - kmin;
		    }
		    erreurMax = hp * sommer;
		    if (erreurMax <= epsilon) {
			break;
		    }
		} else {
		    // assert( i==kmin+1 || i==kmin);
		    break;
		}
	    }
	}
	//
	return phi;
    }

    /**
     * Calcul de proba(N<=nanbNon), selon la loi hypergeometrique Efficace si
     * nanbNon est le plus petit effectif de contre-exemple Choix de la
     * modelisation de A=>B (nanbNon) par la regle al�atoire A=>Y (N)
     */
    private static double sommeProbaAY(final int n, final int na, final int nb,
	    final int nanbNon, Pointer hp_nanbNon, final double erreurMax) {
	// cas na >= nbNon
	// calcul de la formule de la force d'implication
	// pour cela, sommation de max(0, na-nb) a min(nanbNon, na, nbNon)

	// conditions de validite
	// assert ( conditionValidite(n, na, nb, nanbNon) );
	// conditions d'efficacite :
	// assert( na <= n-nb && na <= nb );
	// assert( n > 1 );

	int precision = 3;
	int min = 3;
	int k0;
	double hp_k0, moy, ectype;
	double intensite;

	moy = (double) na * (n - nb) / n;
	ectype = Math.sqrt(moy * (nb * (n - na) / n) / (n - 1));

	k0 = (int) Math.floor(moy);
	if (moy - k0 > 0.5) {
	    k0++;
	}
	k0 = Math.min(k0, nanbNon);

	// 1 Calcul hp(k0)
	hp_k0 = hypergeometrique(n, na, nb, k0);
	Pointer.setValue(hp_k0);
	if ((hp_k0 == 0.0) || (hp_k0 <= DBL_MIN) || (hp_k0 >= DBL_MAX)) {
	    // d�passement de la plus petite (grande) valeur !!!
	    // cerr << "**** line: " << __LINE__ << " file: " << __FILE__ ;
	    // cerr << " hp(n=" << n << ",na=" << na << ",nb=" << nb << ",k0="
	    // << k0 << ")= " << hp_k0 << " ! ";
	    // cerr << endl;
	    // assert( hp_k0 <= 1.0 );
	    return hp_k0;
	}

	// 2 calcul de l'intensite
	intensite = sommeHpInv(n, na, nb, erreurMax, 0, k0, hp_k0);
	if (k0 + 1 <= nanbNon) {
	    intensite += sommeHp(n, na, nb, erreurMax, k0 + 1, nanbNon,
		    hpSuivant(n, na, nb, k0 + 1, hp_k0), hp_nanbNon);
	}

	// assert( hp_k0 <= 1.0 );
	// assert( *hp_nanbNon <= 1.0 );

	return intensite;
    }

    /**
     * Calcul de proba(N<=nanbNon), selon la loi hypergeometrique Efficace si
     * nanbNon est le plus petit effectif de contre-exemple Choix la meilleur
     * modelisation de A=>B (nanbNon) : A=>Y ou X=>B (N)
     */
    private static double sommeProba(final int n, final int na, final int nb,
	    final int nanbNon, Pointer hp_nanbNon, final double erreurMax) {
	int naNonnb = nb - (na - nanbNon);
	int nab = na - nanbNon;

	// assert( nanbNon>=max(0,int(na-nb)) && nanbNon<=min(na,n-nb) );
	// assert( na <= nb && nanbNon <= nab );

	// Calcul de phi(A=>B)
	if (na <= (n - nb)) {
	    // cas ou na <= n-nb : f0 mod�lisation A=>Y de A=>B
	    return sommeProbaAY(n, na, nb, nanbNon, hp_nanbNon, erreurMax);
	} else {
	    // cas ou na > n-nb : f1 mod�lisation X=>B de A=>B
	    return sommeProbaAY(n, n - nb, n - na, nanbNon, hp_nanbNon,
		    erreurMax);
	}
    }

    /**
     * Calcul de l'intensite d'implication phi(A=>B) selon la loi
     * hypergeometrique Avec phi(A=>B) = 1 - proba(N<=nanbNon) = 1 -
     * somme{max(0,int(na-nb), min(na,n-nb)} (N=nanbNon)
     * 
     * NB: phi(A=>B) = phi(B=>A) = phi(Anon=>Bnon) = phi(Bnon=>Anon)
     */
    private static double intensiteImplication(final int n, final int na,
	    final int nb, final int nanbNon, Pointer hp_nanbNon,
	    final double erreurMax) {
	int nab = na - nanbNon;
	int naNonnb = nb - nab;
	int naNonnbNon = n - nb - nanbNon;
	int minnab;
	double somme;

	// assert( nanbNon>=max(0,int(na-nb)) && nanbNon<=min(na,n-nb) );
	// assert( nab>=0 && naNonnb>=0 && naNonnbNon>=0 && nanbNon>=0);

	// Choix du plus petit effectif de contre-exemples
	minnab = Math
		.min(Math.min(nanbNon, naNonnb), Math.min(nab, naNonnbNon));
	if (minnab == nanbNon) {
	    // nanbNon est le plus petit effectif
	    // assert(na<=nb); // nanbNon <= naNonnb
	    // Calcul de phi(A=>B) = 1-proba(N<=nanbNon) : f0 ou f1
	    return 1.0 - sommeProba(n, na, nb, nanbNon, hp_nanbNon, erreurMax);
	}
	if (minnab == naNonnb) {
	    // assert( nb <= na );
	    // naNonnb est le plus petit effectif
	    // Calcul de phi(A=>B) = phi(B=>A) = 1-proba(N<=naNonnb) : f2 ou f3
	    return 1.0 - sommeProba(n, nb, na, naNonnb, hp_nanbNon, erreurMax);
	}
	if (minnab == nab) {
	    // assert( na <= n-nb );
	    // nab est le plus petit effectif
	    // Calcul de phi(A=>B) = 1 - phi(A=>Bnon) - proba(N=nab) =
	    // proba(N<nab) : f4 ou f5
	    if (nab == 0)
		return 0.0;
	    somme = sommeProba(n, na, n - nb, nab - 1, hp_nanbNon, erreurMax);
	    Pointer.setValue(hpSuivant(n, na, n - nb, nab, Pointer.getValue()));
	    //
	    return somme;
	}
	if (minnab == naNonnbNon) {
	    // assert( na >= n-nb );
	    // Calcul de phi(A=>B) = 1 - phi(Bnon=>A) - proba(N=naNonnbNon) =
	    // proba(N<naNonnbNon) : f6 ou f7
	    if (naNonnbNon == 0)
		return 0.0;
	    somme = sommeProba(n, n - nb, na, naNonnbNon - 1, hp_nanbNon,
		    erreurMax);
	    Pointer.setValue(hpSuivant(n, n - nb, na, naNonnbNon, Pointer
		    .getValue()));
	    return somme;
	}
	// assert(false);
	System.err
		.println("************** EREUR GRAVE !!!! **********************************");
	return 100000;
    }

    /**
     * Calcul de l'intensite d'implication phi(A=>Bnon) selon la loi
     * hypergeometrique NB: phi(A=>Bnon) = phi(Bnon=>A) = phi(Anon=>B) =
     * phi(B=>Anon)
     */
    private static double intensiteImplicationABnon(final double iiAB,
	    final double hp_nanbNon) {
	// Calcul de phi(A=>Bnon) = 1 - phi(A=>B) - proba(N=nab)
	return 1.0 - iiAB - hp_nanbNon;
    }

    /**
     * 
     * hyper-geometric distribution
     */
    public static double hypergeometric(final double n, final double na,
	    final double nb, final double nab_) {
	double erreurMax = 1.0e-10;
	Pointer hp_nanbNon = new Pointer(0);
	//
	return intensiteImplication((int) n, (int) na, (int) nb, (int) nab_,
		hp_nanbNon, erreurMax);
    }

    /**
     * 
     * adjustement information
     */
    private static double iba(final double n, final double na, final double nb,
	    final double nab_, final double alpha) {
	double fa = na / n;
	double fab_ = nab_ / n;
	//
	if ((fab_ > 0) && (fab_ < fa / 2))
	    return 1 + Math.pow((1 - fab_ / fa) * Math.log(1 - fab_ / fa)
		    + (fab_ / fa) * Math.log(fab_ / fa), alpha);
	return 0;
    }

    /**
     * 
     * adjustement information
     */
    private static double ia_b_(final double n, final double na,
	    final double nb, final double nab_, final double alpha) {
	double fab_ = nab_ / n;
	double fb_ = (n - nb) / n;
	//
	if ((fab_ > 0) && (fab_ < fb_ / 2))
	    return 1 + Math.pow((1 - fab_ / fb_) * Math.log(1 - fab_ / fb_)
		    + (fab_ / fb_) * Math.log(fab_ / fb_), alpha);
	return 0;
    }

    /**
     * 
     * inclusion index
     */
    public static double inclusion(final double n, final double na,
	    final double nb, final double nab_, final double alpha) {
	return Math.pow(iba(n, na, nb, nab_, alpha)
		* ia_b_(n, na, nb, nab_, alpha), (1 / (2 * alpha)));
    }

    /**
     * 
     * entropic version
     */
    public static double entropic(final double n, final double na,
	    final double nb, final double nab_, final double alpha) {
	return Math.sqrt(hypergeometric(n, na, nb, nab_)
		* inclusion(n, na, nb, nab_, alpha));
    }

    @Override
    public double getIndice(int na, int nb, int nab, int n) {
	return entropic(n, na, nb, na - nab, 1);
    }

    /**
     *
     * 
     */
    private static double E(double x) {
	if (x == 0)
	    return 0.0;
	if ((x > 0.0) && (x <= 0.5))
	    return -x * Math.log(x) - (1.0 - x) * Math.log(1.0 - x);
	if ((x > 0.5) && (x <= 1.0))
	    return 1.0;
	//
	return 0.0; // not used, just for compilation
    }

    /**
     *
     * 
     */
    private static double TI1(final double n, final double na, final double nb,
	    final double nab_) {
	double nb_ = n - nb;
	//
	if (nb_ == 0.0)
	    return Double.NaN;
	return 1.0 - E(nab_ / na) / E(nb_ / n);
    }

    /**
     *
     * 
     */
    private static double TI2(final double n, final double na, final double nb,
	    final double nab_) {
	double na_b = nb - (na - nab_);
	double na_ = n - na;
	double nb_ = n - nb;
	//
	if (nb_ == 0.0)
	    return Double.NaN;
	return 1.0 - E(na_b / nb) / E(na_ / n);
    }

    /**
     *
     * 
     */
    public static double tic(final double n, final double na, final double nb,
	    final double nab_) {
	double TIab = TI1(n, na, nb, nab_);
	double TIb_a_ = TI2(n, na, nb, nab_);
	//
	if ((TIab < 0.0) || (TIb_a_ < 0.0))
	    return 0.0;
	return Math.sqrt(TIab * TIb_a_);
    }

    /**
     *
     *
     */
    public static double factorial(final double n) {
	double result = 1;

	if (n <= 15) {
	    // result = 1;
	    for (int k = 0; k <= n; k++) {
		result = result * k;
	    }
	} else {
	    result = Math.pow(n, n) * Math.exp(-n) * Math.sqrt(2 * Math.PI * n);
	}
	return result;
    }

    /**
     *
     *
     */
    public static double sumCombinaison(final double n, final double na,
	    final double nb, final double nab_) {
	double sum = 0;
	//
	for (int k = 0; k < nab_; k++) {
	    sum += factorial(na) / (factorial(k) * factorial(na - k));
	}
	return sum;
    }

    /**
     *
     * 
     */
    public static double ipee(final double n, final double na, final double nb,
	    final double nab_) {
	double sum = 0;
	double coef = 1 / Math.pow(2, na);
	//
	for (int k = 0; k < nab_; k++) {
	    double fna = factorial(na);
	    double fk = factorial(k);
	    double fnak = factorial(na - k);
	    double ifact = 0;

	    ifact = fna / fk;
	    ifact /= fnak;
	    // = factorial(na)/(factorial((double)k)*factorial(na-(double)k));
	    sum += ifact / coef;

	    // (1/Math.pow(2, na)*sumCombinaison(n,na,nb,nab_));
	}
	return 1 - sum;
    }
}
