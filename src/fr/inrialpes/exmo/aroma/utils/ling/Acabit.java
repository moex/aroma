/**
 *   Copyright 2007, 2008 Jérôme DAVID, Université de Nantes, INRIA, Université Pierre Mendès France
 *   
 *   Acabit.java is part of AROMA.
 *
 *   AROMA is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   AROMA is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with AROMA; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
package fr.inrialpes.exmo.aroma.utils.ling;

/**
 * @author jerome
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

public class Acabit implements TermExtractor {

    private static String TERM_TYPE = "binary";

    private static String ACABIT_HOME = "acabit_en";

    private static String HEADER = "<record>\n<AN></AN>\n<TI></TI>\n<AB><ph_nb=1>\n";

    private static String FOOTER = " <ph>\n</AB>\n</record>";

    private static String ACABIT_TEMP_FILE;

    private static String ACABIT_RES_FILE = "REA/temp2.txt";

    private File getInputFile(String lemmatisedText) {
	File tempFile;
	try {
	    tempFile = new File(ACABIT_HOME + "/acabit.txt");// File.
	    // createTempFile
	    // ("acabit"
	    // ,"txt");
	    // System.out.println(tempFile.getAbsolutePath());
	    FileWriter fw = new FileWriter(tempFile);
	    fw.write(HEADER);
	    fw.write(lemmatisedText);
	    fw.write(FOOTER);
	    fw.close();
	    return tempFile;
	} catch (IOException e) {
	    e.printStackTrace();
	}
	return null;
    }

    public Set<String> getTerms(String lemmatisedText) {
	try {
	    File f = getInputFile(lemmatisedText);
	    callAcabit(f);
	    return readResultFile();

	} catch (Exception e) {
	    e.printStackTrace();
	}
	return null;
    }

    /*
     * private Collection getTerms() { File terms = new File(); }
     */

    private void callAcabit(File f) throws Exception {
	// Runtime.getRuntime().exec(new String[] {"bash","cd",ACABIT_HOME});
	Runtime.getRuntime().exec(
		new String[] { "perl", "en_stat.pl", f.getAbsolutePath() },
		null, new File(ACABIT_HOME)).waitFor();
	Runtime.getRuntime().exec(new String[] { "perl", "en_tri.pl" }, null,
		new File(ACABIT_HOME)).waitFor();
    }

    private Set<String> readResultFile() throws IOException {
	InputStream is = new FileInputStream(ACABIT_HOME + "/"
		+ ACABIT_RES_FILE);
	BufferedReader d = new BufferedReader(new InputStreamReader(is));
	Set<String> terms = new HashSet<String>();
	while (d.ready()) {
	    String line = d.readLine();
	    // System.out.println();
	    String term = line.split(" ([0-9]+ )?---")[0];
	    // System.out.println(term);

	    terms.add(term);
	}
	// System.out.println("coucou");
	return terms;
    }

    public String getTermType() {
	return TERM_TYPE;
    }

    /* (non-Javadoc)
     * @see fr.inrialpes.exmo.aroma.utils.ling.TermExtractor#getTerms(java.lang.String, java.lang.String)
     */
    public Set<String> getTerms(String text, String language) {
	return getTerms(text);
    }

    public void free() {
    }

}
